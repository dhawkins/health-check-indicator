--
-- @version $Id$
-- @copyright 1999, 2016, ShoreGroup, Inc.
--

--
-- Updates OIDs
--

USE CaseSentry;

INSERT IGNORE INTO `snmpPluginDef` VALUES ('4-6_HealthCheck-Application','.....4.6 Health Check: Application','.1.3.6.1.4.1.2021.110.1.3.1.4.16.116.101.115.116.45.97.112.112.108.105.99.97.116.105.111.110.3.1.4.5.115.104.101.108.108','2','','1;0;unknown','F','NoMachine','snmp','NONE',''); 

INSERT IGNORE INTO `snmpPluginDef` VALUES ('4-6_HealthCheck-Backups','.....4.6 Health Check: Backups','.1.3.6.1.4.1.2021.90.1.3.1.4.12.116.101.115.116.45.98.97.99.107.117.112.115.3.1.4.5.115.104.101.108.108','2','','1;0;unknown','F','NoMachine','snmp',' NONE',''); 

INSERT IGNORE INTO `snmpPluginDef` VALUES ('4-6_HealthCheck-Database','.....4.6 Health Check: Database','.1.3.6.1.4.1.2021.80.1.3.1.4.13.116.101.115.116.45.100.97.116.97.98.97.115.101.3.1.4.5.115.104.101.108.108','2','','1;0;unknown','F','NoMachine','snmp',' NONE','');

INSERT IGNORE INTO `snmpPluginDef` VALUES ('4-6_HealthCheck-Processes','.....4.6 Health Check: Processes','.1.3.6.1.4.1.2021.120.1.3.1.4.14.116.101.115.116.45.112.114.111.99.101.115.115.101.115.3.1.4.5.115.104.101.108.108','2','','1;0;unknown','F','NoMachine','snmp','NONE',''); 

INSERT IGNORE INTO `snmpPluginDef` VALUES ('4-6_HealthCheck-Queues','.....4.6 Health Check: Queues','.1.3.6.1.4.1.2021.100.1.3.1.4.12.116.101.115.116.115.45.113.117.101.117.101.115.3.1.4.5.115.104.101.108.108','2','','1;0;unknown','F','NoMachine','snmp','NONE',''); 

INSERT IGNORE INTO `snmpPluginDef` VALUES ('4-6_HealthCheck-System','.....4.6 Health Check: System','.1.3.6.1.4.1.2021.70.1.3.1.4.11.116.101.115.116.45.115.121.115.116.101.109.3.1.4.5.115.104.101.108.108','2','','1;0;unknown','F','NoMachine','snmp','NONE',''); 

INSERT IGNORE INTO `snmpPluginDef` VALUES ('Application','Application Healthcheck','.1.3.6.1.4.1.2021.110.1.3.1.4.5.115.104.101.108.108','2','','1;unknown','F','NoMachine','snmp','NONE','');

INSERT IGNORE INTO `snmpPluginDef` VALUES ('Database','Database Healthcheck','.1.3.6.1.4.1.2021.80.1.3.1.4.5.115.104.101.108.108','2','','1','F','NoMachine','snmp','NONE','');

INSERT IGNORE INTO `snmpPluginDef` VALUES ('Healthcheck-localbackups','Healthcheck-localbackups','.1.3.6.1.4.1.2021.90.1.100.1','2','','1','F','NoMachine','snmp','NONE','');

INSERT IGNORE INTO `snmpPluginDef` VALUES ('Healthcheck-replication','Healthcheck-replication','.1.3.6.1.4.1.2021.80.1.100.1','2','','1','F','NoMachine','snmp','NONE',''); 

INSERT IGNORE INTO `snmpPluginDef` VALUES ('Healthcheck-system','Healthcheck-system','.1.3.6.1.4.1.2021.70.1.100.1','2','','0;1','F','NoMachine','snmp','NONE','');

INSERT IGNORE INTO `snmpPluginDef` VALUES ('Healthcheck-Ubuntuapplica','Healthcheck-Ubuntuapplication','.1.3.6.1.4.1.2021.110.1.3.1.4.5.115.104.101.108.108','2','','1;unknown','F','NoMachine','snmp','NONE',''); 

INSERT IGNORE INTO `snmpPluginDef` VALUES ('Healthcheck-Ubuntubackups','Healthcheck-localbackups Ubuntu','.1.3.6.1.4.1.2021.90.1.3.1.4.5.115.104.101.108.108','2','','1','F','NoMachine','snmp','NONE',''); 

INSERT IGNORE INTO `snmpPluginDef` VALUES ('Healthcheck-Ubuntuprocess','Healthcheck-Ubuntuprocesses','.1.3.6.1.4.1.2021.120.1.3.1.4.5.115.104.101.108.108','2','','1;unknown','F','NoMachine','snmp','NONE',''); 

INSERT IGNORE INTO `snmpPluginDef` VALUES ('Healthcheck-Ubuntuqueues','Healthcheck-Ubutnuqueues','.1.3.6.1.4.1.2021.100.1.3.1.4.5.115.104.101.108.108','2','','1;unknown','F','NoMachine','snmp','NONE',''); 

INSERT IGNORE INTO `snmpPluginDef` VALUES ('Healthcheck-Ubuntureplica','Healthcheck-replication Ubuntu','.1.3.6.1.4.1.2021.80.1.3.1.4.5.115.104.101.108.108','2','','1','F','NoMachine','snmp','NONE','');

INSERT IGNORE INTO `snmpPluginDef` VALUES ('Healthcheck-Ubuntusystem','Healthcheck-system Ubuntu','.1.3.6.1.4.1.2021.70.1.3.1.4.5.115.104.101.108.108','2','','1','F','NoMachine','snmp','NONE',''); 

INSERT IGNORE INTO `snmpPluginDef` VALUES ('LocalBackup','LocalBackup Healthcheck Ubuntu','.1.3.6.1.4.1.2021.90.1.3.1.4.5.115.104.101.108.108','2','','1','F','NoMachine','snmp','NONE','');

INSERT IGNORE INTO `snmpPluginDef` VALUES ('Processes','Processes Healthcheck','.1.3.6.1.4.1.2021.120.1.3.1.4.5.115.104.101.108.108','2','','1;unknown','F','NoMachine','snmp','NONE','');

INSERT IGNORE INTO `snmpPluginDef` VALUES ('Queues','Queues Healthcheck','.1.3.6.1.4.1.2021.100.1.3.1.4.5.115.104.101.108.108','2','','1;unknown','F','NoMachine','snmp','NONE',''); 

INSERT IGNORE INTO `snmpPluginDef` VALUES ('System','System Healthcheck','.1.3.6.1.4.1.2021.70.1.3.1.4.5.115.104.101.108.108','2','','1','F','NoMachine','snmp','NONE','');
