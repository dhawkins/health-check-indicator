# functions used by sendHealthEmail.pl
#
# @version $Id: $
# @copyright 1999,2016, ShoreGroup, Inc.

package sendHealthEmail;

use strict;
use warnings;
use 5.010;

use lib '/usr/lib/health-indicators/perl';
use vars qw($VERSION @ISA @EXPORT);
$VERSION = 1.0;

our @ISA = qw(Exporter);

our @EXPORT = qw(sendEmail);

# This function determines if a health check email should be sent
# An email will be sent to CCS or CMAP when a health check fails and a lock file will be created
# An email will be sent to CCS or CMAP for the first passing health checks after a failure and the lock file will be removed
# An email will NOT be sent to CCS or CMAP if the health check passes and the lock file does not exist
#
sub sendEmail($$) {
    my ($returnCode, $lockFile) = @_;

    my $status = 'pass';

    if ($returnCode == 1) {
        if ( -e $lockFile ) {
            $status = 'warning';  # health check failed and lock file exists so trigger email
        }
        else {
            $status = 'critical';
            `/bin/date > $lockFile`; # health check failed but no lock file so create it and trigger email
        }
    }
    else {
        if ( -e $lockFile ) {
                `/bin/rm $lockFile`;
                 $status = 'clear'; # health check passed and lock file exists so clear it and trigger email
        }
    }

    return $status;
}

1;
