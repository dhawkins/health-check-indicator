#
# $Id: ConnectVars.pm 68875 2014-11-14 21:29:00Z smcdonald $
# Copyright 1999,2013, ShoreGroup, Inc.
# #################################

package ConnectVars;
require Exporter;
$| = 1;

use strict;
use lib '/var/www/CaseSentry/lib/Perl';
use DBI qw(:sql_types);
use Constants;
use ConfigTable;
use Frob qw(frob);
use printDebug;
use XML::Parser;
use XML::DOM;
use MIME::Base64;
use JSON;
use Data::Dumper;

my $DEBUG = 0;
my $iReturnValue = 1;

our @ISA = qw(Exporter);
our @EXPORT = qw(getConnectVars getConnection getCdrServerConnection getCallCenterReportParams extractUserFromSession getAttributes $CS_CONNECT_USER $CS_CONNECT_PW $CS_CONNECT_DB $CS_CONNECT_HOST $SAMPSON_DB $SAMPSON_DB_SERVER $SAMPSON_USER $SAMPSON_PASSWORD $STATUS_DB_SERVER $STATUS_DB_NAME $STATUS_DB_USER $STATUS_DB_PASSWORD $OBJECT_DB_SERVER $OBJECT_DB_NAME $OBJECT_DB_USER $OBJECT_DB_PASSWORD $LOG_DB_SERVER $LOG_DB_NAME $LOG_DB_USER $LOG_DB_PASSWORD $DECISION_ENGINE_DB_SERVER $DECISION_ENGINE_DB_NAME $DECISION_ENGINE_DB_USER $DECISION_ENGINE_DB_PASSWORD $footColor $footTextColor $DEFAULT_EMAIL $QPAGE $DIGEST_PAGE_THRESHOLD $DIGEST_CMS_MSG_THRESHOLD $sSUPPORTADDRESS $sSUPPORTSERVER $bNOTIFYSUPPORT $BASE_URL $sSYSTEM_TYPE $sREDUNDANCY_IP $sREDUNDANCY_ALIAS $sPRIMARY_IP $sSECONDARY_IP $TIMEOUT $gsDEFAULT_CASE_PRIORITY $bCriticalObjectStatus $sStatusTableFormat $iStatusRefreshTimeout $sSiteName $bConsolidateCaseNotifications $sSAMPSON_ROOT $bUSE_REVERSE_LOOKUP $FORCE_STATUS_WRITE $NO_FORCE_STATUS_WRITE 
$STATUS_TYPE_RAW $STATUS_TYPE_DEPEND $STATUS_TYPE_DECISION $STATUS_TYPE_CASE $STATUS_TYPE_OUTAGE $STATUS_RAW_CRITICAL $STATUS_RAW_NORMAL $STATUS_DEPEND_CRITICAL $STATUS_DEPEND_NORMAL $STATUS_DEPEND_DEPEND_DOWN $STATUS_DECISION_CRITICAL $STATUS_DECISION_NORMAL $STATUS_CASE_NONE $STATUS_CASE_HIGH $STATUS_CASE_MEDIUM $STATUS_CASE_LOW $STATUS_CASE_INFO $STATUS_CASE_MONITOR_ONLY $STATUS_OUTAGE_NORMAL $STATUS_OUTAGE_ACTIVE $TKT_LIST_ROW_COLOR1 $TKT_LIST_ROW_COLOR2 $gbCHECK_SESSION_IP $sDMZ_IP $bOPEN_THRESHOLD_CASE $bEMAIL_THRESHOLD_NOTIFY $iOBJ_INACTIVE $iOBJ_MONITORED $iOBJ_NON_MONITORED $sPUBLIC_KEY $sTIMEZONE $gsNOTIF_HTML_FORMAT $gsNOTIF_NO_SUBJECT $gsNOTIF_INCLUDE_SUBJECT $gsNOTIF_SUPPRESS_SUBJECT $gsNOTIF_CONSOLIDATE $sREPORT_FROM_ADDRESS $gsNOTIF_PREFIX_ONLY_SUBJECT $SECONDARY_DMZ_IP );

our ($CS_CONNECT_USER, $CS_CONNECT_PW, $CS_CONNECT_DB, $CS_CONNECT_HOST);
our $SAMPSON_DB;
our $SAMPSON_DB_SERVER;
our $SAMPSON_USER;
our $SAMPSON_PASSWORD;

our $STATUS_DB_SERVER;
our $STATUS_DB_NAME;
our $STATUS_DB_USER;
our $STATUS_DB_PASSWORD;

our $OBJECT_DB_SERVER;
our $OBJECT_DB_NAME;
our $OBJECT_DB_USER;
our $OBJECT_DB_PASSWORD;

our $LOG_DB_SERVER;
our $LOG_DB_NAME;
our $LOG_DB_USER;
our $LOG_DB_PASSWORD;

our $MRTG_DB_SERVER;
our $MRTG_DB_NAME;
our $MRTG_DB_USER;
our $MRTG_DB_PASSWORD;

our $DECISION_ENGINE_DB_SERVER;
our $DECISION_ENGINE_DB_NAME;
our $DECISION_ENGINE_DB_USER;
our $DECISION_ENGINE_DB_PASSWORD;

our $footColor;
our $footTextColor;

our $TIMEOUT;
our $gsDEFAULT_CASE_PRIORITY;
our $bCriticalObjectStatus;
our $sStatusTableFormat;
our $iStatusRefreshTimeout;
our $sSiteName;
our $bConsolidateCaseNotifications;

our $DEFAULT_EMAIL;
our $QPAGE;
our $DIGEST_PAGE_THRESHOLD;
our $DIGEST_CMS_MSG_THRESHOLD;
our $sSUPPORTADDRESS;
our $sSUPPORTSERVER;
our $bNOTIFYSUPPORT;
our $BASE_URL;
our $sSYSTEM_TYPE;
our $sREDUNDANCY_IP;
our $sREDUNDANCY_ALIAS;
our $sPRIMARY_IP;
our $sSECONDARY_IP;
our $sSAMPSON_ROOT;
our $bUSE_REVERSE_LOOKUP;
our $FORCE_STATUS_WRITE;
our $NO_FORCE_STATUS_WRITE;
our $STATUS_TYPE_RAW;
our $STATUS_TYPE_DEPEND;
our $STATUS_TYPE_DECISION;
our $STATUS_TYPE_CASE;
our $STATUS_TYPE_OUTAGE;
our $STATUS_RAW_CRITICAL;
our $STATUS_RAW_NORMAL;
our $STATUS_DEPEND_CRITICAL;
our $STATUS_DEPEND_NORMAL;
our $STATUS_DEPEND_DEPEND_DOWN;
our $STATUS_DECISION_CRITICAL;
our $STATUS_DECISION_NORMAL;
our $STATUS_CASE_NONE;
our $STATUS_CASE_HIGH;
our $STATUS_CASE_MEDIUM;
our $STATUS_CASE_LOW;
our $STATUS_CASE_INFO;
our $STATUS_CASE_MONITOR_ONLY;
our $STATUS_OUTAGE_NORMAL;
our $STATUS_OUTAGE_ACTIVE;
our $TKT_LIST_ROW_COLOR1;
our $TKT_LIST_ROW_COLOR2;
our $gbCHECK_SESSION_IP;
our $sDMZ_IP;
our $SECONDARY_DMZ_IP;
our $bOPEN_THRESHOLD_CASE;
our $bEMAIL_THRESHOLD_NOTIFY;
our $sPUBLIC_KEY;
our $iOBJ_INACTIVE;
our $iOBJ_MONITORED;
our $iOBJ_NON_MONITORED;
our $sTIMEZONE;
our $gsNOTIF_TEXT_FORMAT;
our $gsNOTIF_HTML_FORMAT;
our $gsNOTIF_NO_SUBJECT;
our $gsNOTIF_INCLUDE_SUBJECT;
our $gsNOTIF_SUPPRESS_SUBJECT;
our $gsNOTIF_PREFIX_ONLY_SUBJECT;
our $gsNOTIF_CONSOLIDATE;
our $sREPORT_FROM_ADDRESS;

## Connection code needs to be in here to get
## a connection before it is needed below.
## This code is not yet written.
##



sub set_db_connection_params
{
my $hCSConnectDb    = shift;
my $sConnectionName = shift;
my $sHostName       = shift;    # Pass by Ref
my $sUser           = shift;    # Pass by Ref
my $sPassword       = shift;    # Pass by Ref
my $sDatabase       = shift;    # Pass by Ref

my ($sQuery, $hSt, $iResult);
my $iStatus = 1;

    $sQuery = "SELECT hostname, user, pw, db FROM db_logins WHERE name = "
                        . $hCSConnectDb->quote($sConnectionName);

    $hSt = $hCSConnectDb->prepare( $sQuery ) or die "Cannot prepare query (Connection Name: $sConnectionName)\n";
    $iResult = $hSt->execute or die "Cannot execute query (Connection Name: $sConnectionName)\n";

    if ($iResult)
    {
        if (!(($$sHostName, $$sUser, $$sPassword, $$sDatabase) = $hSt->fetchrow_array))
        {
            $iStatus = 0;
        }
    }

    return $iStatus;
}

#*******************************************
# getConnectVars
#*******************************************
sub getConnectVars
{

    my $bReturnValue = 0;
    if (open INFILE, "/var/www/CaseSentry/include/PHP/connect_vars.inc")
    {
        $bReturnValue = 1;
    }

    while (<INFILE>)
    {
        $footColor = $1 if /\$FOOTER_COLOR\s*=.*([#][a-fA-F0-9]{1,6})/ ;
        $footTextColor = $1 if /\$FOOTER_TEXT_COLOR\s*=.*([#][a-fA-F0-9]{1,6})/ ;
        $DIGEST_PAGE_THRESHOLD = $1 if (/\$DIGEST_PAGE_THRESHOLD\s*=\s*([0-9]+);/);
        $DIGEST_CMS_MSG_THRESHOLD = $1 if (/\$DIGEST_CMS_MSG_THRESHOLD\s*=\s*([0-9]+);/);
        $TKT_LIST_ROW_COLOR1 = $1 if /\$TKT_LIST_ROW_COLOR1\s*=.*([#][a-fA-F0-9]{1,6})/ ;
        $TKT_LIST_ROW_COLOR2 = $1 if /\$TKT_LIST_ROW_COLOR2\s*=.*([#][a-fA-F0-9]{1,6})/ ;
    }

    getDbConnectionSettings();

    close INFILE;

    my $hCSConnectDb = DBI->connect("DBI:mysql:$CS_CONNECT_DB:$CS_CONNECT_HOST","$CS_CONNECT_USER","$CS_CONNECT_PW") or die "Database connection not made: $DBI::errstr";

    # This is needed for legacy code.
    # Get/Set DB variables
    set_db_connection_params(
            $hCSConnectDb, "Main",
            \$SAMPSON_DB_SERVER, \$SAMPSON_USER, \$SAMPSON_PASSWORD, \$SAMPSON_DB );

    set_db_connection_params(
            $hCSConnectDb, "Status",
            \$STATUS_DB_SERVER, \$STATUS_DB_USER, \$STATUS_DB_PASSWORD, \$STATUS_DB_NAME );

    set_db_connection_params(
            $hCSConnectDb, "Object",
            \$OBJECT_DB_SERVER, \$OBJECT_DB_USER, \$OBJECT_DB_PASSWORD, \$OBJECT_DB_NAME );

    set_db_connection_params(
            $hCSConnectDb, "Log",
            \$LOG_DB_SERVER, \$LOG_DB_USER, \$LOG_DB_PASSWORD, \$LOG_DB_NAME );

    set_db_connection_params(
            $hCSConnectDb, "MrtgData",
            \$MRTG_DB_SERVER, \$MRTG_DB_USER, \$MRTG_DB_PASSWORD, \$MRTG_DB_NAME );

    set_db_connection_params(
            $hCSConnectDb, "DecisionEngine",
            \$DECISION_ENGINE_DB_SERVER, \$DECISION_ENGINE_DB_USER, \$DECISION_ENGINE_DB_PASSWORD, \$DECISION_ENGINE_DB_NAME );

    # Connect to Main DB here
    my $hDb = DBI->connect("DBI:mysql:$SAMPSON_DB:$SAMPSON_DB_SERVER","$SAMPSON_USER","$SAMPSON_PASSWORD") or die "Database connection not made: $DBI::errstr";


    # new method to get constants
    #
    $iOBJ_INACTIVE               = getConstant($hDb, 'OBJ_INACTIVE');
    $iOBJ_MONITORED              = getConstant($hDb, 'OBJ_MONITORED');
    $iOBJ_NON_MONITORED          = getConstant($hDb, 'OBJ_NON_MONITORED');
    $gsNOTIF_TEXT_FORMAT         = getConstant($hDb, 'NOTIF_TEXT_FORMAT', 'Notifications');
    $gsNOTIF_HTML_FORMAT         = getConstant($hDb, 'NOTIF_HTML_FORMAT', 'Notifications');
    $gsNOTIF_NO_SUBJECT          = getConstant($hDb, 'NOTIF_NO_SUBJECT', 'Notifications');
    $gsNOTIF_INCLUDE_SUBJECT     = getConstant($hDb, 'NOTIF_INCLUDE_SUBJECT', 'Notifications');
    $gsNOTIF_SUPPRESS_SUBJECT    = getConstant($hDb, 'NOTIF_SUPPRESS_SUBJECT', 'Notifications');
    $gsNOTIF_PREFIX_ONLY_SUBJECT = getConstant($hDb, 'NOTIF_PREFIX_ONLY_SUBJECT', 'Notifications');
    $gsNOTIF_CONSOLIDATE         = getConstant($hDb, 'NOTIF_CONSOLIDATE', 'Notifications');
    $QPAGE                       = getConstant($hDb, 'QPAGE', 'File Paths');

    #   Get CaseSentryConfig values
    #
    $sSAMPSON_ROOT      = getCSConfigValue( $hDb, 'sSAMPSON_ROOT' );
    $gbCHECK_SESSION_IP = getCSConfigValue( $hDb, 'Check Session IP', 'Sessions' );
    $sTIMEZONE          = getCSConfigValue( $hDb, 'sTIMEZONE' );
    $bUSE_REVERSE_LOOKUP   = getCSConfigValue($hDb, "Export:RME:Use Reverse Lookup", "Network Objects");
    $FORCE_STATUS_WRITE    = getCSConfigValue($hDb, "bFORCE_STATUS_WRITE");
    $NO_FORCE_STATUS_WRITE = getCSConfigValue($hDb, "bNO_FORCE_STATUS_WRITE");
    $DEFAULT_EMAIL      = getCSConfigValue($hDb, "Default", "EMail");
    $sREPORT_FROM_ADDRESS = getCSConfigValue($hDb, "SUPPORT_FROM_EMAIL", "EMail");
    getConfigParms();
    setStatusTypeVariables($hDb);
    setStatusLevelInfoVariables($hDb);

    if ($ENV{QUERY_STRING})
    {
        my $sUser = '';
        my @sAttriNames = ('timeout','Default Case Priority'); ##contains attribute names to retrieve

        $iReturnValue = extractUserFromSession(\$sUser);
        ($TIMEOUT, $gsDEFAULT_CASE_PRIORITY) = getAttributes(\@sAttriNames, \$sUser);
    }
#       ($TIMEOUT, $gsDEFAULT_CASE_PRIORITY, $bCriticalObjectStatus, $sStatusTableFormat, $iStatusRefreshTimeout, $sSiteName, $bConsolidateCaseNotifications) = getAttributes();

    return  $bReturnValue;
}

sub getConfigParms
{
    my $hDb = '';
    my $sQuery = '';
    my $hStatement = '';
    my $iResult = 0;
    my $sConfigParm = '';
    my $sConfigValue = '';

    #get user login name from database
    $hDb = DBI->connect("DBI:mysql:$SAMPSON_DB:$SAMPSON_DB_SERVER","$SAMPSON_USER","$SAMPSON_PASSWORD") or die "Database connection not made: $DBI::errstr";

    $sQuery = "SELECT parm, value FROM CaseSentryConfig";
    $hStatement= $hDb->prepare($sQuery) or die "Cannot prepare $sQuery: $hDb->errstr\n";
    $iResult = $hStatement->execute or die "Cannot execute query: $hStatement->errstr\n";

    if ($iResult)
    {
        while (($sConfigParm, $sConfigValue) = $hStatement->fetchrow_array)
        {
            # I know this is inefficient, but for now...
            $BASE_URL = $sConfigValue if ($sConfigParm eq 'BASE_URL');
            $sSYSTEM_TYPE = $sConfigValue if ($sConfigParm eq 'sSYSTEM_TYPE');
            $sPRIMARY_IP = $sConfigValue if ($sConfigParm eq 'sPRIMARY_IP');
            $sSECONDARY_IP = $sConfigValue if ($sConfigParm eq 'sSECONDARY_IP');
            $sREDUNDANCY_IP = $sConfigValue if ($sConfigParm eq 'sREDUNDANCY_IP');
            $sREDUNDANCY_ALIAS = $sConfigValue if ($sConfigParm eq 'sREDUNDANCY_ALIAS');
            $sDMZ_IP = $sConfigValue if ($sConfigParm eq 'sDMZ_IP');
            $SECONDARY_DMZ_IP = $sConfigValue if ($sConfigParm eq 'SECONDARY_DMZ_IP');
            $sSUPPORTSERVER = $sConfigValue if ($sConfigParm eq 'sSUPPORTSERVER');
            $bNOTIFYSUPPORT = $sConfigValue if ($sConfigParm eq 'bNOTIFYSUPPORT');
            $sSUPPORTADDRESS = $sConfigValue if ($sConfigParm eq 'sSUPPORTADDRESS');
            $bOPEN_THRESHOLD_CASE = $sConfigValue if ($sConfigParm eq 'bOPEN_THRESHOLD_CASE');
            $bEMAIL_THRESHOLD_NOTIFY = $sConfigValue if ($sConfigParm eq 'bEMAIL_THRESHOLD_NOTIFY');
            $sPUBLIC_KEY = $sConfigValue if ($sConfigParm eq 'sPUBLIC_KEY');
        }
    }

    $hStatement->finish();
    $hDb->disconnect();

}

sub extractUserFromSession
{
    my $hUser = shift;

    #get Session Key first:
    my @aTokenValPairs = split /&/, $ENV{QUERY_STRING};

    my $dbh = "";
    my $sql = "";
    my $sth = "";
    my $rv = "";
    my $sSessionID = "";
    my $token_val = "";
    my $iReturnValue = 1;

    foreach $token_val (@aTokenValPairs)
    {
        if ($token_val =~ /SessionKey=/)
        {
            $token_val =~ s/SessionKey=//;
            $sSessionID = $token_val;
        }
    }

    if (!defined $sSessionID)
    {
        $sSessionID = '';
    }

    #get user login name from database
    $dbh = DBI->connect("DBI:mysql:$SAMPSON_DB:$SAMPSON_DB_SERVER","$SAMPSON_USER","$SAMPSON_PASSWORD") or die "Database connection not made: $DBI::errstr";

    $sql = "SELECT login FROM sessions WHERE session_id = '$sSessionID' AND originating_ip_ipvx = '$ENV{REMOTE_ADDR}'";
    $sth= $dbh->prepare($sql) or die "Cannot prepare $sql: $dbh->errstr\n";
    $rv = $sth->execute or die "Cannot execute query: $sth->errstr\n";

    if ($rv)
    {
        ($$hUser) = $sth->fetchrow_array;
    }
    else
    {
        $iReturnValue = 0;
    }

    $sth->finish();
    $dbh->disconnect();

    return $iReturnValue;
}

sub getAttributes
{
    my $hAttriNames = shift;
    my $hUser = shift;

    my @returnVal = ('');
    my $dbh = "";
    my $sql = "";
    my $sth = "";
    my $rc = "";
    my $rv = "";
    my ( %sysVal, %userVal, %locks );
    my ($token_val, $sSessionID);
    my ( $sAttrName, $sAttrValue, $sLogin, $bLock);

    #get user login name from database
    $dbh = DBI->connect("DBI:mysql:$SAMPSON_DB:$SAMPSON_DB_SERVER","$SAMPSON_USER","$SAMPSON_PASSWORD") or die "Database connection not made: $DBI::errstr";

    ###get timeout value from database
    $sql = "SELECT attr_name, attr_value, login, lock_attr FROM preferences WHERE login IN ('~SYSTEM', '$hUser')";
    $sth= $dbh->prepare($sql) or die "Cannot prepare $sql: $dbh->errstr\n";
    $rv = $sth->execute or die "Cannot execute query: $sth->errstr\n";
    if ( $rv )
    {
        while ( my @row = $sth->fetchrow_array )
        {

            ($sAttrName, $sAttrValue, $sLogin, $bLock) = @row;

            if ( $sLogin eq '~SYSTEM' )
            {
                $sysVal{ $sAttrName } =  $sAttrValue;
                $locks{ $sAttrName } = $bLock;
                # set userVal to an empty string to avoid an undef error
                $userVal{ $sAttrName } =  '';
            }
            else
            {
                $userVal{ $sAttrName } =  $sAttrValue;
            }
        }

        for( my $i = 0; $i < scalar @$hAttriNames; $i++ )
        {
            if (  ( $locks{ $$hAttriNames[ $i ] } eq "F" ) && ( $userVal{ $$hAttriNames[ $i ] }  || $userVal{ $$hAttriNames[ $i ] } eq '0' ) )
            {
                $returnVal[ $i ] =  $userVal{ $$hAttriNames[ $i ] };
            }
            else
            {
                $returnVal[ $i ] =  $sysVal{ $$hAttriNames[ $i ] };
            }
        }
    }

    $sth->finish();
    $dbh->disconnect();

    return @returnVal;

}

sub getMySQLtimeZone()
{
    my $dbh = "";
    my $sql = "";
    my $sth = "";
    my $rc = "";
    my $rv = "";

    $dbh = DBI->connect("DBI:mysql:$SAMPSON_DB:$SAMPSON_DB_SERVER","$SAMPSON_USER","$SAMPSON_PASSWORD") or die "Database connection not made: $DBI::errstr";
    $sql = "show variables like 'timezone'";
    $sth = $dbh->prepare($sql) or die "Cannot prepare $sql: $dbh->errstr\n";
    $rv = $sth->execute or die "Cannot execute query: $sth->errstr\n";
    if ( $rv )
    {
        my @sTZ = $sth->fetchrow_array;
        my $rc = $sth->finish();
        $rc = $dbh->disconnect();
        return $sTZ[1];
    }
}

sub getNamedDBConnection($)
{
    my $connection = shift;

    local $/ = undef;
    my $configFile = "/etc/CaseSentry/db_logins.conf";
    open CONFIGFILE, $configFile or die ("Unable to open database connection info file: " . $configFile . "\n");
    my $confData = <CONFIGFILE>;
    my $jsonData = from_json($confData);
    if ($jsonData->{$connection}) {
      if ($DEBUG == 1) {
        warn "[DEBUG]: " . Dumper($jsonData->{$connection}) . "\n"; 
      }
        return $jsonData->{$connection};

    }
    else {
        die "Requested look up of: " . $connection . " but that does not exist in the config file: " . $configFile . " . falling back to database lookup.";
    }
}

sub getDbConnectionSettings
{
    my $parser = new XML::DOM::Parser;
    my $config = $parser->parsefile("/var/www/CaseSentry/config/db.cfg");
    my @db = $config->getDocumentElement()->getElementsByTagName("db", 0);
    my $db = shift(@db);
    my $children = $db->getChildNodes();
    my $len = $children->getLength();
    for (my $i = 0; $i < $len; $i++) {
        my $child = $children->item($i);
        my $name = $child->getNodeName();
        if ($name eq "user") {
            my $value = $child->getFirstChild()->getNodeValue();
            $CS_CONNECT_USER = frob(decode_base64($value));
        }
        elsif ($name eq "host") {
            my $value = $child->getFirstChild()->getNodeValue();
            $CS_CONNECT_HOST = frob(decode_base64($value));
        }
        elsif ($name eq "password") {
            my $value = $child->getFirstChild()->getNodeValue();
            $CS_CONNECT_PW = frob(decode_base64($value));
        }
        elsif ($name eq "db") {
            my $value = $child->getFirstChild()->getNodeValue();
            $CS_CONNECT_DB = frob(decode_base64($value));
        }
    }
    $config->dispose;
}

sub setStatusTypeVariables
{
    my $hDb = shift;
    my $sSql = '';
    my $iStatusTypeId = 0;
    my $sStatusType = '';
    my $sStatusTypeVarName = '';
    my $sStatusTypeDesc = '';
    my $hStatement = '';
    my $iResult = 0;

    $sSql = "SELECT id, var_name FROM status_type";
    $hStatement= $hDb->prepare($sSql) or die "Cannot prepare $sSql: $hDb->errstr\n";
    $iResult = $hStatement->execute or die "Cannot execute query: $hStatement->errstr\n";

    while ( my ($iID, $sVarName) = $hStatement->fetchrow_array )
    {
        VAR_SWITCH:
        {
            $sVarName eq 'STATUS_TYPE_RAW' && do
            {
                $STATUS_TYPE_RAW = $iID;

                last VAR_SWITCH;
            };

            $sVarName eq 'STATUS_TYPE_DEPEND' && do
            {
                $STATUS_TYPE_DEPEND = $iID;

                last VAR_SWITCH;
            };

            $sVarName eq 'STATUS_TYPE_DECISION' && do
            {
                $STATUS_TYPE_DECISION = $iID;

                last VAR_SWITCH;
            };

            $sVarName eq 'STATUS_TYPE_CASE' && do
            {
                $STATUS_TYPE_CASE = $iID;

                last VAR_SWITCH;
            };

            $sVarName eq 'STATUS_TYPE_OUTAGE' && do
            {
                $STATUS_TYPE_OUTAGE = $iID;

                last VAR_SWITCH;
            };

            printDebug("Unknown status type variable in VAR_SWITCH");
        }
    }

    $hStatement->finish();
}

sub setStatusLevelInfoVariables
{
    my $hDb = shift;
    my $sSql = '';
    my $hStatement = '';
    my $iResult = 0;

    $sSql = "SELECT status_level, var_name FROM status_level";
    $hStatement= $hDb->prepare($sSql) or die "Cannot prepare $sSql: $hDb->errstr\n";
    $iResult = $hStatement->execute or die "Cannot execute query: $hStatement->errstr\n";

    while ( my ($sStatusLevel, $sVarName) = $hStatement->fetchrow_array )
    {
        VAR_SWITCH:
        {
            $sVarName eq 'STATUS_RAW_CRITICAL' && do
            {
                $STATUS_RAW_CRITICAL = $sStatusLevel;

                last VAR_SWITCH;
            };

            $sVarName eq 'STATUS_RAW_NORMAL' && do
            {
                $STATUS_RAW_NORMAL = $sStatusLevel;

                last VAR_SWITCH;
            };

            $sVarName eq 'STATUS_DEPEND_CRITICAL' && do
            {
                $STATUS_DEPEND_CRITICAL = $sStatusLevel;

                last VAR_SWITCH;
            };

            $sVarName eq 'STATUS_DEPEND_NORMAL' && do
            {
                $STATUS_DEPEND_NORMAL = $sStatusLevel;

                last VAR_SWITCH;
            };

            $sVarName eq 'STATUS_DEPEND_DEPEND_DOWN' && do
            {
                $STATUS_DEPEND_DEPEND_DOWN = $sStatusLevel;

                last VAR_SWITCH;
            };

            $sVarName eq 'STATUS_DECISION_CRITICAL' && do
            {
                $STATUS_DECISION_CRITICAL = $sStatusLevel;

                last VAR_SWITCH;
            };

            $sVarName eq 'STATUS_DECISION_NORMAL' && do
            {
                $STATUS_DECISION_NORMAL = $sStatusLevel;

                last VAR_SWITCH;
            };

            $sVarName eq 'STATUS_CASE_NONE' && do
            {
                $STATUS_CASE_NONE = $sStatusLevel;

                last VAR_SWITCH;
            };

            $sVarName eq 'STATUS_CASE_HIGH' && do
            {
                $STATUS_CASE_HIGH = $sStatusLevel;

                last VAR_SWITCH;
            };

            $sVarName eq 'STATUS_CASE_MEDIUM' && do
            {
                $STATUS_CASE_MEDIUM = $sStatusLevel;

                last VAR_SWITCH;
            };

            $sVarName eq 'STATUS_CASE_LOW' && do
            {
                $STATUS_CASE_LOW = $sStatusLevel;

                last VAR_SWITCH;
            };

            $sVarName eq 'STATUS_CASE_INFO' && do
            {
                $STATUS_CASE_INFO = $sStatusLevel;

                last VAR_SWITCH;
            };

            $sVarName eq 'STATUS_CASE_MONITOR_ONLY' && do
            {
                $STATUS_CASE_MONITOR_ONLY = $sStatusLevel;

                last VAR_SWITCH;
            };

            $sVarName eq 'STATUS_OUTAGE_NORMAL' && do
            {
                $STATUS_OUTAGE_NORMAL = $sStatusLevel;

                last VAR_SWITCH;
            };

            $sVarName eq 'STATUS_OUTAGE_ACTIVE' && do
            {
                $STATUS_OUTAGE_ACTIVE = $sStatusLevel;

                last VAR_SWITCH;
            };

            printDebug("Unknown status level variable in VAR_SWITCH");
        }
    }

    $hStatement->finish();

}

# return a database handle for the connection $name
sub getConnection
{
    my $name   = shift || 'Main';
    my $dbh    = 0;
    my $result = 0;
    my @params;

    # connect through cs_conn
    my $connectionInfo = getNamedDBConnection($name);
    if (defined($connectionInfo)) {
        $dbh = DBI->connect("DBI:mysql:"
        . $connectionInfo->{'db'} . ":" . $connectionInfo->{'hostname'},
        $connectionInfo->{'user'},
        $connectionInfo->{'pw'},
        {
        	'mysql_enable_utf8' => 1
	    })
        or die "Database connection not made: $DBI::errstr";

    	$dbh->do('SET NAMES utf8');
    }
    return $dbh;
}

# Connect to the CCM 5 CDR server
#
# @param string $database Database name
# @param resource $dbMain Database connection handle
# @return resource
sub getCdrServerConnection($$)
{
    my $database = shift;
    my $dbMain = shift;

    # Get connection details from the Main database
    my $server = getCSConfigValue($dbMain, 'CDR Server', 'CCM 5');
    my $user = getCSConfigValue($dbMain, 'CDR Server admin login', 'CCM 5');
    my $password = getCSConfigValue($dbMain, 'CDR Server admin password', 'CCM 5');

    # Connect to the database
    my $connection = DBI->connect(
        sprintf('DBI:mysql:%s:%s', $database, $server),
        $user,
        $password
    ) or die("Could not connect to CDR Server: " . $server . "\n");

    return $connection;
}

# Returns as hash of connection parameters for call center reports.
# Connection params are taken from ccr_* tables.
# I would not call myself a perl programmer so try not to make fun.
sub getCallCenterReportParams($)
{
    my $report = shift;
    my $dsname = shift;
    my $dbh    = getConnection("Main");
    my $result = 0;
    my $index  = 0;
    my %params = {};
    my @row;

    my $query = "
        SELECT
          `ccr_reports`.`report_name`,
          `ccr_datasources`.`datasource_name`,
          `ccr_datasources`.`hostname`,
          `ccr_datasources`.`database`,
          `ccr_datasources`.`username`,
          `ccr_datasources`.`password`,
          `ccr_datasource_types`.`type`
        FROM `ccr_reports`
        LEFT JOIN `ccr_reports_datasources` ON `ccr_reports_datasources`.`report_name` = `ccr_reports`.`report_name`
        LEFT JOIN `ccr_datasources` ON `ccr_datasources`.`datasource_name` = `ccr_reports_datasources`.`datasource_name`
        LEFT JOIN `ccr_datasource_types` ON `ccr_datasource_types`.`id` = `ccr_datasources`.`datasource_type`
        WHERE `ccr_reports`.`report_name` = '$report'; ";

    my $statement = $dbh->prepare($query);
    if ($result = $statement->execute()) {
        while(@row = $statement->fetchrow_array()) {
            # index by datasource name
            $params{$row[1]} = {
                'report_name'     => $row[0],
                'datasource_name' => $row[1],
                'hostname'        => $row[2],
                'database'        => $row[3],
                'username'        => $row[4],
                'password'        => $row[5],
                'type'            => $row[6]
            };
        }
    }

    return %params;
}

1;

