#
# CS_Diagnostics
#
# @todo add a detailed description of the file here...
#
# @version $Id: CS_Diagnostics.pm 63079 2014-08-11 13:34:59Z amiller $
# @copyright 1999,2011, ShoreGroup, Inc.

package CS_Diagnostics;

use strict;
use lib '/var/www/CaseSentry/lib/Perl';
use ConnectVars;
use vars qw($VERSION @ISA @EXPORT);
use CS_Error;
use Carp;
use Data::Dumper;
use DBI qw(:sql_types);
use ConfigTable qw( getCSConfigValue );

$VERSION = 1.0;

our @ISA = qw(Exporter);
our @EXPORT = qw(
									getActiveStateMachineCount
									getAuthCheck
									getAvgCaseUpdateTime
									getAvgDcTime
									getAvgSspTime
									getCaseSentryConfig
									getCaseSentryConfigAttribute
									getCaseSentryProcessStatus
									getCaseSentryStatus
									getFailoverFlagsAttribute
									getLastCaseUpdateTime
									getLastDcTime
									getLastMrtgUpdateTime
									getLastObjectStatusUpdateTime
									getLastSspTime
									getMasterStatus
									getSlaveStatus
									getSystemFailoverFlags
									getSystemFailoverFlagsAttribute
									getSystemLayout
									setFailoverFlags
									setSystemFailoverFlags
									getDiagnosticErrorNumber
									getDiagnosticErrorString
									setDiagnosticError
									isCSLicenseActive
                                    isActive
								);

#####################################
# getActiveStateMachineCount
#####################################
sub getActiveStateMachineCount
{
	my $hDb = shift;
	my $hActiveMachineCount = shift;

	my $sQuery = '';
	my $hStatement = '';
	my $iResult = 0;
	my $iReturnValue = 1;

	$sQuery = "SELECT COUNT(*) FROM current_state WHERE state_name <> 'Ground' AND machine <> 'slidingWindow'";
	$hStatement = $$hDb->prepare($sQuery);

	if (defined($hStatement))
	{
		$iResult = $hStatement->execute();

		if ($iResult)
		{
			# put records in array form processing
			($$hActiveMachineCount) = $hStatement->fetchrow_array;

			$hStatement->finish();
		}
		else
		{
			setDiagnosticError(DIAG_ACTIVE_STATE_MACHINE_COUNT_DB_ERROR);
			$$hActiveMachineCount = undef;
			$iReturnValue = 0;
		}
	}
	else
	{
		setDiagnosticError(DIAG_ACTIVE_STATE_MACHINE_COUNT_DB_ERROR);
		$$hActiveMachineCount = undef;
		$iReturnValue = 0;
	}

	return $iReturnValue;

}

#####################################
# getAuthCheck
#####################################
sub getAuthCheck
{
	my $hAuthCheck = shift;

	my $bFound = 0;
	my $iReturnValue = 1;
	my $iCommandResult = 0;

	$iCommandResult = open AC, "/var/www/CaseSentry/bin/AuthCheck |";

	if (defined($iCommandResult))
	{
		while (<AC>)
		{
			if (/success/i && !$bFound)
			{
				$$hAuthCheck = 'success';
				$bFound = 1;
			}
			elsif (/failure/i && !$bFound)
			{
				$$hAuthCheck = 'failure';
				$bFound = 1;
			}
		}

		close AC;

		if (!$bFound)
		{
			setDiagnosticError(DIAG_UNKNOWN_AUTHCHECK_RESULT);
			$iReturnValue = DIAG_UNKNOWN_AUTHCHECK_RESULT->[0];
			$$hAuthCheck = undef;
		}
	}
	else
	{
		setDiagnosticError(DIAG_UNABLE_TO_EXEC_PROCESS);
		$iReturnValue = DIAG_UNABLE_TO_EXEC_PROCESS->[0];
		$$hAuthCheck = undef;
	}

	return $iReturnValue;

}

#####################################
# getAvgCaseUpdateTime
#####################################
sub getAvgCaseUpdateTime
{
	my $hDb = shift;
	my $hDays = shift;
	my $hSampleSize = shift;
	my $hApproximate = shift;
	my $hAvgPollTime = shift;

	my $sQuery = '';
	my $hStatement = '';
	my $iResult = 0;
	my $iReturnValue = 1;
	my $iRowNum = 0;
	my $iSeconds = 0;
	my @sRecords = ();
	my @iTicketNumbers = ();
	my $iTicketNum = 0;
	my $iRecordCount = 0;
	my $iElapsedSeconds = 0;
	my @iCaseAverages = ();
	my $iNumDeltas = 0;
	my $iAvgCount = 0;
	my $iAvgTotal = 0;
	my $iNumCases = 0;
	my $iCaseNumDelta = 0;
	my $iSampleCount = 0;
	my $iMinCaseNum = 0;
	my $iMaxCaseNum = 0;
	my $iTicketCount = 0;

	# get number of cases opened in last x days
	$sQuery = "SELECT MIN(ticket_num), MAX(ticket_num) FROM ticket WHERE create_date >= DATE_SUB(NOW(),INTERVAL $$hDays DAY)";
	$hStatement = $$hDb->prepare($sQuery);

	if (defined($hStatement))
	{
		$iResult = $hStatement->execute();

		if ($iResult)
		{
			@iTicketNumbers = $hStatement->fetchrow_array;
			if (defined($iTicketNumbers[0]) && defined($iTicketNumbers[1]))
			{
				$iMinCaseNum = $iTicketNumbers[0];
				$iMaxCaseNum = $iTicketNumbers[1];
				$iNumCases = $iTicketNumbers[1] - $iTicketNumbers[0];
				$iCaseNumDelta = sprintf("%1.0f", int($iNumCases / ($$hSampleSize - 1)));
			}

			if ((defined($iTicketNumbers[0]) && ($iNumCases >= $$hSampleSize)))
			{
				# clear array
				@iTicketNumbers = ();

				# collect ticket numbers
				for ($iSampleCount = 0; $iSampleCount < $$hSampleSize; $iSampleCount++)
				{
					push @iTicketNumbers, $iMinCaseNum + ($iSampleCount * $iCaseNumDelta);
				}
			}
			else  # can't get samsple size in number of days specified.
			{
				# clear array
				@iTicketNumbers = ();

				if ($$hApproximate ne lc('DAYS') &&
						$$hApproximate ne lc('SAMPLE') &&
						$$hApproximate ne lc('BOTH'))
				{
					setDiagnosticError(DIAG_SAMPLE_DAYS_TOO_SMALL);
					$iReturnValue = DIAG_SAMPLE_DAYS_TOO_SMALL->[0];
					$$hAvgPollTime = undef;
				}
				else
				{
					APPROX_SWITCH:
					{
						$$hApproximate eq lc('DAYS') && do
						{
							$sQuery = "SELECT COUNT(ticket_num) FROM ticket";
							$hStatement = $$hDb->prepare($sQuery);

							if (defined($hStatement))
							{
								$iResult = $hStatement->execute();

								if ($iResult)
								{
									($iTicketCount) = $hStatement->fetchrow_array;
									if ($iTicketCount >= $$hSampleSize)
									{
										$sQuery = "SELECT ticket_num FROM ticket ORDER BY id DESC LIMIT 0, $$hSampleSize";
										$hStatement = $$hDb->prepare($sQuery);

										if (defined($hStatement))
										{
											$iResult = $hStatement->execute();

											if ($iResult)
											{
												while (($iTicketNum) = $hStatement->fetchrow_array)
												{
													push @iTicketNumbers, $iTicketNum;
												}
											}
											else
											{
												setDiagnosticError(DIAG_AVG_CASE_UPDATE_TIME_DB_ERROR);
												$iReturnValue = 0;
												$$hAvgPollTime = undef;
											}
										}
										else
										{
											setDiagnosticError(DIAG_AVG_CASE_UPDATE_TIME_DB_ERROR);
											$iReturnValue = 0;
											$$hAvgPollTime = undef;
										}
									}
									else
									{
										setDiagnosticError(DIAG_SAMPLE_EXCEEDS_TOTAL_CASES);
										$iReturnValue = DIAG_SAMPLE_EXCEEDS_TOTAL_CASES->[0];
										$$hAvgPollTime = undef;
									}
								}
								else
								{
									setDiagnosticError(DIAG_AVG_CASE_UPDATE_TIME_DB_ERROR);
									$iReturnValue = 0;
									$$hAvgPollTime = undef;
								}
							}
							else
							{
								setDiagnosticError(DIAG_AVG_CASE_UPDATE_TIME_DB_ERROR);
								$iReturnValue = 0;
								$$hAvgPollTime = undef;
							}

							last APPROX_SWITCH;
						};

						$$hApproximate eq lc('SAMPLE') && do
						{
							$sQuery = "SELECT COUNT(ticket_num) FROM ticket WHERE create_date >= DATE_SUB(NOW(),INTERVAL $$hDays DAY)";
							$hStatement = $$hDb->prepare($sQuery);

							if (defined($hStatement))
							{
								$iResult = $hStatement->execute();

								if ($iResult)
								{
									($iTicketCount) = $hStatement->fetchrow_array;
									if ($iTicketCount >= 1)
									{
										$sQuery = "SELECT MIN(ticket_num), MAX(ticket_num) FROM ticket WHERE create_date >= DATE_SUB(NOW(),INTERVAL $$hDays DAY)";
										$hStatement = $$hDb->prepare($sQuery);

										if (defined($hStatement))
										{
											$iResult = $hStatement->execute();

											if ($iResult)
											{
												while (($iTicketNum) = $hStatement->fetchrow_array)
												{
													push @iTicketNumbers, $iTicketNum;
												}
											}
											else
											{
												setDiagnosticError(DIAG_AVG_CASE_UPDATE_TIME_DB_ERROR);
												$iReturnValue = 0;
												$$hAvgPollTime = undef;
											}
										}
										else
										{
											setDiagnosticError(DIAG_AVG_CASE_UPDATE_TIME_DB_ERROR);
											$iReturnValue = 0;
											$$hAvgPollTime = undef;
										}
									}
									else
									{
										setDiagnosticError(DIAG_TOTAL_CASES_ZERO);
										$iReturnValue = DIAG_TOTAL_CASES_ZERO->[0];
										$$hAvgPollTime = undef;
									}
								}
								else
								{
									setDiagnosticError(DIAG_AVG_CASE_UPDATE_TIME_DB_ERROR);
									$iReturnValue = 0;
									$$hAvgPollTime = undef;
								}
							}
							else
							{
								setDiagnosticError(DIAG_AVG_CASE_UPDATE_TIME_DB_ERROR);
								$iReturnValue = 0;
								$$hAvgPollTime = undef;
							}

							last APPROX_SWITCH;
						};

						$$hApproximate eq lc('BOTH') && do
						{
							$sQuery = "SELECT ticket_num FROM ticket";
							$hStatement = $$hDb->prepare($sQuery);

							if (defined($hStatement))
							{
								$iResult = $hStatement->execute();

								if ($iResult)
								{
									while (($iTicketNum) = $hStatement->fetchrow_array)
									{
										push @iTicketNumbers, $iTicketNum;
									}
								}
								else
								{
									setDiagnosticError(DIAG_AVG_CASE_UPDATE_TIME_DB_ERROR);
									$iReturnValue = 0;
									$$hAvgPollTime = undef;
								}
							}
							else
							{
								setDiagnosticError(DIAG_AVG_CASE_UPDATE_TIME_DB_ERROR);
								$iReturnValue = 0;
								$$hAvgPollTime = undef;
							}

							last APPROX_SWITCH;
						};
					}
				}
			}

			if (scalar(@iTicketNumbers) >= 1)
			{
				$iTicketNum = 0;
				foreach $iTicketNum (@iTicketNumbers)
				{
					$sQuery = "SELECT UNIX_TIMESTAMP(create_date) FROM ticket_text WHERE ticket_num=$iTicketNum ORDER BY id";
					$hStatement = $$hDb->prepare($sQuery);

					if (defined($hStatement))
					{
						$iResult = $hStatement->execute();

						if ($iResult)
						{
							@sRecords = ();
							# put records in array form processing
							while (($iSeconds) = $hStatement->fetchrow_array)
							{
								push @sRecords, $iSeconds;
							}

							# save those with at least one time delta
							$iElapsedSeconds = 0;
							$iNumDeltas = scalar(@sRecords) - 1;
							if ($iNumDeltas > 0)
							{
								for ($iRecordCount = 0; $iRecordCount < $iNumDeltas; $iRecordCount++)
								{
									$iElapsedSeconds = $sRecords[$iRecordCount + 1] - $sRecords[$iRecordCount];
								}

								push @iCaseAverages, ($iElapsedSeconds / $iNumDeltas);
							}

							# if there are cases with averages
							if (scalar(@iCaseAverages) >= 1)
							{
								for ($iAvgCount = 0; $iAvgCount < scalar(@iCaseAverages); $iAvgCount++)
								{
									$iAvgTotal += $iCaseAverages[$iAvgCount];
								}

								$$hAvgPollTime = sprintf("%.2f", ($iAvgTotal / scalar(@iCaseAverages)));
							}
							else
							{
								setDiagnosticError(DIAG_NO_CASE_AVERAGE);
								$iReturnValue = DIAG_NO_CASE_AVERAGE->[0];
								$$hAvgPollTime = undef;
							}
						}
						else
						{
							setDiagnosticError(DIAG_AVG_CASE_UPDATE_TIME_DB_ERROR);
							$iReturnValue = 0;
							$$hAvgPollTime = undef;
						}
					}
					else
					{
						setDiagnosticError(DIAG_AVG_CASE_UPDATE_TIME_DB_ERROR);
						$iReturnValue = 0;
						$$hAvgPollTime = undef;
					}
				}
			}
			else
			{
				setDiagnosticError(DIAG_NO_CASE_NUMBERS);
				$iReturnValue = DIAG_NO_CASE_NUMBERS->[0];
				$$hAvgPollTime = undef;
			}
		}
		else
		{
			setDiagnosticError(DIAG_AVG_CASE_UPDATE_TIME_DB_ERROR);
			$$hAvgPollTime = undef;
			$iReturnValue = 0;
		}

		$hStatement->finish();
	}
	else
	{
		setDiagnosticError(DIAG_AVG_CASE_UPDATE_TIME_DB_ERROR);
		$$hAvgPollTime = undef;
		$iReturnValue = 0;
	}

	return $iReturnValue;

}

#####################################
# getAvgDcTime
#####################################
sub getAvgDcTime
{
	my $hDb = shift;
	my $hAvgPollTime = shift;

	my $sProcess = '';
	my $iReturnValue = 1;

	$sProcess = 'DependChecker';

	$iReturnValue = getAvgPollTime($hDb, \$sProcess, $hAvgPollTime);
	if ($iReturnValue != 1)
	{
		$$hAvgPollTime = undef;
	}

	return $iReturnValue;
}

#####################################
# getAvgSspTime
#####################################
sub getAvgSspTime
{
	my $hDb = shift;
	my $hAvgPollTime = shift;

	my $sProcess = '';
	my $iReturnValue = 1;

	$sProcess = 'SnapshotStatusPoller';
	$iReturnValue = getAvgPollTime($hDb, \$sProcess, $hAvgPollTime);
	if ($iReturnValue != 1)
	{
		$$hAvgPollTime = undef;
	}

	return $iReturnValue;
}

#####################################
# getCaseSentryConfig
#####################################
sub getCaseSentryConfig
{
	my $hDb = shift;
	my $hCaseSentryConfig = shift;

	my $sQuery = '';
	my $hStatement = '';
	my $iResult = 0;
	my $iReturnValue = 1;
	my $sParm = '';
	my $sValue = '';

	$sQuery = "SELECT parm, value FROM CaseSentryConfig";
	$hStatement = $hDb->prepare($sQuery);

	if (defined($hStatement))
	{
		$iResult = $hStatement->execute();

		if ($iResult)
		{
			while (($sParm,$sValue) = $hStatement->fetchrow_array)
			{
				$$hCaseSentryConfig{$sParm} = $sValue;
			}

			$hStatement->finish();
		}
		else
		{
			setDiagnosticError(DIAG_CASESENTRY_CONFIG_DB_ERROR);
			$iReturnValue = 0;
			%$hCaseSentryConfig = ();
		}
	}
	else
	{
		setDiagnosticError(DIAG_CASESENTRY_CONFIG_DB_ERROR);
		$iReturnValue = 0;
		%$hCaseSentryConfig = ();
	}

	return $iReturnValue;
}

#####################################
# getCaseSentryConfigAttribute
#####################################
sub getCaseSentryConfigAttribute
{
	my $hDb = shift;
	my $hConfigAttribute = shift;
	my $hConfigValue = shift;

	my $sQuery = '';
	my $hStatement = '';
	my $iResult = 0;
	my $iReturnValue = 1;

	$sQuery = "SELECT value FROM CaseSentryConfig WHERE parm='$$hConfigAttribute'";
	$hStatement = $$hDb->prepare($sQuery);

	if (defined($hStatement))
	{
		$iResult = $hStatement->execute();

		if ($iResult)
		{
			($$hConfigValue) = $hStatement->fetchrow_array;

			$hStatement->finish();

			# check to see if blank
			if ($$hConfigValue eq '')
			{
				setDiagnosticError(DIAG_CONFIG_ATTRIBUTE_NOT_FOUND);
				$iReturnValue = DIAG_CONFIG_ATTRIBUTE_NOT_FOUND->[0];
				$$hConfigValue = undef;
			}
		}
		else
		{
			setDiagnosticError(DIAG_CASESENTRY_CONFIG_ATTRIBUTE_DB_ERROR);
			$iReturnValue = 0;
			$$hConfigValue = undef;
		}
	}
	else
	{
		setDiagnosticError(DIAG_CASESENTRY_CONFIG_ATTRIBUTE_DB_ERROR);
		$iReturnValue = 0;
		$$hConfigValue = undef;
	}

	return $iReturnValue;
}

#####################################
# getCaseSentryProcessStatus
#####################################
sub getCaseSentryProcessStatus
{
	my $hProcess = shift;
	my $hStatus = shift;

	my %sCaseSentryStatus = ();
	my $iReturnValue = 1;

	# get all statuses
	$iReturnValue = getCaseSentryStatus(\%sCaseSentryStatus);

	if ($iReturnValue)
	{
		# check for wanted status
		if ($sCaseSentryStatus{$$hProcess})
		{
			$$hStatus = $sCaseSentryStatus{$$hProcess};
		}
		else
		{
			setDiagnosticError(DIAG_PROCESS_STATUS_NOT_FOUND);
			$iReturnValue = DIAG_PROCESS_STATUS_NOT_FOUND->[0];
			$$hStatus = undef;
		}
	}

	return $iReturnValue;

}

#####################################
# getCaseSentryStatus
#####################################
sub getCaseSentryStatus
{
	my $hCaseSentryStatus = shift;

	my $sProcess = '';
	my $sStatus = '';
	my $iReturnValue = 1;
	my $iCommandResult = 0;

	$iCommandResult = open CSS, "/var/www/CaseSentry/bin/CaseSentryStatus.pl |";

	if (defined($iCommandResult))
	{
		while (<CSS>)
		{
			if (/^Process:\s+(.+)/)
			{
				$sProcess = $1;
			}

			if (/^Status:\s+(.+)/)
			{
				$sStatus = $1;
			}

			if (/^$/)
			{
				if ($sProcess ne '')
				{
					$$hCaseSentryStatus{$sProcess} = $sStatus;
				}

				$sProcess = '';
				$sStatus = '';
			}
		}

		close CSS;

		# if not defined, explicitly make undef
		if (!scalar(keys(%{$hCaseSentryStatus})))
		{
			setDiagnosticError(DIAG_PROCESS_STATUS_NOT_FOUND);
			$iReturnValue = DIAG_PROCESS_STATUS_NOT_FOUND->[0];
			%$hCaseSentryStatus = ();
		}
	}
	else
	{
		setDiagnosticError(DIAG_UNABLE_TO_EXEC_PROCESS);
		$iReturnValue = DIAG_UNABLE_TO_EXEC_PROCESS->[0];
		%$hCaseSentryStatus = ();
	}

	return $iReturnValue;

}



#####################################
# getFailoverFlagsAttribute
#####################################
sub getFailoverFlagsAttribute
{
	my $hDb = shift;
	my $hAttribute = shift;
	my $hValue = shift;

	my $sQuery = '';
	my $hStatement = '';
	my $iResult = 0;
	my $iReturnValue = 1;

	$sQuery = "SELECT $$hAttribute FROM failover_flags";
	$hStatement = $$hDb->prepare($sQuery);

	if (defined($hStatement))
	{
		$iResult = $hStatement->execute();

		if ($iResult)
		{
			($$hValue) = $hStatement->fetchrow_array;
			$hStatement->finish();
		}
		else
		{
			setDiagnosticError(DIAG_FAILOVER_FLAGS_ATTRIBUTE_DB_ERROR);
			$$hValue = undef;
			$iReturnValue = 0;
		}
	}
	else
	{
		setDiagnosticError(DIAG_FAILOVER_FLAGS_ATTRIBUTE_DB_ERROR);
		$$hValue = undef;
		$iReturnValue = 0;
	}

	return $iReturnValue;
}

#####################################
# getLastCaseUpdateTime
#####################################
sub getLastCaseUpdateTime
{
	my $hDb = shift;
	my $hLastUpdateTime = shift;

	my $sQuery = '';
	my $hStatement = '';
	my $iResult = 0;
	my $iReturnValue = 1;
	my $iSeconds = 0;

	$sQuery = "SELECT UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(MAX(create_date)) FROM ticket_text";
	$hStatement = $$hDb->prepare($sQuery);

	if (defined($hStatement))
	{
		$iResult = $hStatement->execute();

		if ($iResult)
		{
			# put records in array form processing
			($$hLastUpdateTime) = $hStatement->fetchrow_array;

			$hStatement->finish();

		}
		else
		{
			setDiagnosticError(DIAG_LAST_CASE_UPDATE_TIME_DB_ERROR);
			$$hLastUpdateTime = undef;
			$iReturnValue = 0;
		}
	}
	else
	{
		setDiagnosticError(DIAG_LAST_CASE_UPDATE_TIME_DB_ERROR);
		$$hLastUpdateTime = undef;
		$iReturnValue = 0;
	}

	return $iReturnValue;
}

#####################################
# getLastDcTime
#####################################
sub getLastDcTime
{
	my $hDb = shift;
	my $hLastPollTime = shift;

	my $sProcess = '';
	my $iReturnValue = 1;

	$sProcess = 'DependChecker';

	$iReturnValue = getLastPollTime($hDb, \$sProcess, $hLastPollTime);
	if ($iReturnValue != 1)
	{
		$$hLastPollTime = undef;
	}

	return $iReturnValue;
}

#####################################
# getLastMrtgUpdateTime
#####################################
sub getLastMrtgUpdateTime
{
	my $hTarget = shift;
	my $hGraph = shift;
	my $hUpdateTime = shift;

	my $iReturnValue = 1;
	my $iCommandResult = 0;
	my $dataDir = getCSConfigValue(getConnection('Main'), 'Data', 'MrtgDirectories');

	$iCommandResult = open RRD, "/usr/bin/rrdtool info $dataDir/$$hTarget/$$hGraph |";

	if (defined($iCommandResult)) {
		while (<RRD>) {
			if (/^last_update\s+=\s+(.+)/) {
				$$hUpdateTime = time - $1;
			}
		}

		close RRD;

		# this is since the shell execs even though rrdtool might not
		if (!$$hUpdateTime) {
			setDiagnosticError(DIAG_UNABLE_TO_EXEC_PROCESS);

			$iReturnValue = DIAG_UNABLE_TO_EXEC_PROCESS->[0];
			$$hUpdateTime = undef;
		}
	}
	else {
		setDiagnosticError(DIAG_UNABLE_TO_EXEC_PROCESS);

		$iReturnValue = DIAG_UNABLE_TO_EXEC_PROCESS->[0];
		$$hUpdateTime = undef;
	}

	return $iReturnValue;
}

#####################################
# getLastObjectStatusUpdateTime
#####################################
sub getLastObjectStatusUpdateTime
{
	my $hDb = shift;
	my $hLastUpdateTime = shift;

	my $sQuery = '';
	my $hStatement = '';
	my $iResult = 0;
	my $iReturnValue = 1;
	my $iSeconds = 0;

	$sQuery = "SELECT UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(MAX(status_date)) FROM object_status";
	$hStatement = $$hDb->prepare($sQuery);

	if (defined($hStatement))
	{
		$iResult = $hStatement->execute();

		if ($iResult)
		{
			# put records in array form processing
			($$hLastUpdateTime) = $hStatement->fetchrow_array;

			$hStatement->finish();

		}
		else
		{
			setDiagnosticError(DIAG_LAST_OBJECT_STATUS_UPDATE_TIME_DB_ERROR);
			$$hLastUpdateTime = undef;
			$iReturnValue = 0;
		}
	}
	else
	{
		setDiagnosticError(DIAG_LAST_OBJECT_STATUS_UPDATE_TIME_DB_ERROR);
		$$hLastUpdateTime = undef;
		$iReturnValue = 0;
	}

	return $iReturnValue;
}

#####################################
# getLastSspTime
#####################################
sub getLastSspTime
{
	my $hDb = shift;
	my $hLastPollTime = shift;

	my $sProcess = '';
	my $iReturnValue = 1;

	$sProcess = 'SnapshotStatusPoller';

	$iReturnValue = getLastPollTime($hDb, \$sProcess, $hLastPollTime);
	if ($iReturnValue != 1)
	{
		$$hLastPollTime = undef;
	}

	return $iReturnValue;
}

#####################################
# getMasterStatus
#####################################
sub getMasterStatus
{
	my $hDb = shift;
	my $hMasterStatus = shift;

	my $sQuery = '';
	my $hStatement = '';
	my $iResult = 0;
	my $iReturnValue = 1;

	$sQuery = "SHOW MASTER STATUS";
	$hStatement = $$hDb->prepare($sQuery);

	if (defined($hStatement))
	{
		$iResult = $hStatement->execute();

		if ($iResult)
		{
			($$hMasterStatus{'File'},$$hMasterStatus{'Position'},$$hMasterStatus{'Binlog_do_db'},$$hMasterStatus{'Binlog_ignore_db'}) = $hStatement->fetchrow_array;
			$hStatement->finish();
		}
		else
		{
			setDiagnosticError(DIAG_MASTER_STATUS_DB_ERROR);
			%$hMasterStatus = ();
			$iReturnValue = 0;
		}
	}
	else
	{
		setDiagnosticError(DIAG_MASTER_STATUS_DB_ERROR);
		%$hMasterStatus = ();
		$iReturnValue = 0;
	}

	return $iReturnValue;
}

#####################################
# getSlaveStatus
#####################################
sub getSlaveStatus
{
	my $hDb = shift;
	my $hSlaveStatus = shift;

	my $sQuery = '';
	my $hStatement = '';
	my $iResult = 0;
	my $iReturnValue = 1;

	$sQuery = "SHOW SLAVE STATUS";
	$hStatement = $$hDb->prepare($sQuery);

	if (defined($hStatement))
	{
		$iResult = $hStatement->execute();

		if ($iResult)
		{
			($$hSlaveStatus{'Master_Host'},$$hSlaveStatus{'Master_User'},$$hSlaveStatus{'Master_Port'},$$hSlaveStatus{'Connect_retry'},$$hSlaveStatus{'Log_File'},,$$hSlaveStatus{'Pos'},$$hSlaveStatus{'Slave_Running'},$$hSlaveStatus{'Replication_do_db'},$$hSlaveStatus{'Replicate_ignore_db'},$$hSlaveStatus{'Last_errno'},$$hSlaveStatus{'Last_error'},$$hSlaveStatus{'Skip_counter'}) = $hStatement->fetchrow_array;
			$hStatement->finish();
		}
		else
		{
			setDiagnosticError(DIAG_SLAVE_STATUS_DB_ERROR);
			%$hSlaveStatus = ();
			$iReturnValue = 0;
		}
	}
	else
	{
		setDiagnosticError(DIAG_SLAVE_STATUS_DB_ERROR);
		%$hSlaveStatus = ();
		$iReturnValue = 0;
	}

	return $iReturnValue;
}

#####################################
# getSystemFailoverFlags
#####################################
sub getSystemFailoverFlags
{
	my $hDb = shift;
	my $hSystemFailoverFlags = shift;

	my $sQuery = '';
	my $hStatement = '';
	my $iResult = 0;
	my $iReturnValue = 1;

	$sQuery = "SELECT active_poller_group, last_known_good_CS, date FROM SystemFailoverFlags";
	$hStatement = $$hDb->prepare($sQuery) or $iReturnValue = 0;

	if (defined($hStatement))
	{
		$iResult = $hStatement->execute() or $iReturnValue = 0;

		if ($iResult)
		{
			($$hSystemFailoverFlags{'active_poller_group'}, $$hSystemFailoverFlags{'last_known_good_CS'}, $$hSystemFailoverFlags{'date'}) = $hStatement->fetchrow_array;
			$hStatement->finish();
		}
		else
		{
			setDiagnosticError(DIAG_SYSTEM_FAILOVER_FLAGS_DB_ERROR);
			%$hSystemFailoverFlags = ();
			$iReturnValue = 0;
		}
	}
	else
	{
		setDiagnosticError(DIAG_SYSTEM_FAILOVER_FLAGS_DB_ERROR);
		%$hSystemFailoverFlags = ();
		$iReturnValue = 0;
	}

	return $iReturnValue;
}

#####################################
# getSystemFailoverFlagsAttribute
#####################################
sub getSystemFailoverFlagsAttribute
{
	my $hDb = shift;
	my $hAttribute = shift;
	my $hValue = shift;

	my $sQuery = '';
	my $hStatement = '';
	my $iResult = 0;
	my $iReturnValue = 1;

	$sQuery = "SELECT $$hAttribute FROM SystemFailoverFlags";
	$hStatement = $$hDb->prepare($sQuery) or $iReturnValue = 0;

	if (defined($hStatement))
	{
		$iResult = $hStatement->execute() or $iReturnValue = 0;

		if ($iResult)
		{
			($$hValue) = $hStatement->fetchrow_array;
			$hStatement->finish();
		}
		else
		{
			setDiagnosticError(DIAG_SYSTEM_FAILOVER_FLAGS_ATTRIBUTE_DB_ERROR);
			$$hValue = undef;
			$iReturnValue = 0;
		}
	}
	else
	{
		setDiagnosticError(DIAG_SYSTEM_FAILOVER_FLAGS_ATTRIBUTE_DB_ERROR);
		$$hValue = undef;
		$iReturnValue = 0;
	}

	return $iReturnValue;
}

#####################################
# getSystemLayout
#####################################
sub getSystemLayout
{
	my $hDb = shift;
	my $hSystemLayout = shift;

	my @sRow = ();
	my $sQuery = '';
	my $hStatement = '';
	my $iResult = 0;
	my $iReturnValue = 1;
	my $sRecordHash = {};

	$sQuery = "SELECT
							ip_addr_ipvx,
							role,
							location,
							location_number,
							group_name,
							group_number,
							sequence_number
						FROM SystemLayout";

	$hStatement = $$hDb->prepare($sQuery);

	if (defined($hStatement))
	{
		$iResult = $hStatement->execute();

		if ($iResult)
		{
			while (@sRow = $hStatement->fetchrow_array)
			{
				$sRecordHash = {'ip_addr'=>$sRow[0],'role'=>$sRow[1],'location'=>$sRow[2],'location_number'=>$sRow[3],'group_name'=>$sRow[4],'group_number'=>$sRow[5],'sequence_number'=>$sRow[6]};
				push @{$hSystemLayout}, $sRecordHash;
			}

			$hStatement->finish();
		}
		else
		{
			setDiagnosticError(DIAG_SYSTEM_LAYOUT_DB_ERROR);
			%$hSystemLayout = ();
			$iReturnValue = 0;
		}
	}
	else
	{
		setDiagnosticError(DIAG_SYSTEM_LAYOUT_DB_ERROR);
		%$hSystemLayout = ();
		$iReturnValue = 0;
	}

	return $iReturnValue;
}

#####################################
# setFailoverFlags
#####################################
sub setFailoverFlags
{
	my $hDb = shift;
	my $hFailoverFlags = shift;

	my $sQuery = '';
	my $hStatement = '';
	my $iResult = 0;
	my $iReturnValue = 1;
	my $sAttribute = '';
	my $sValue = '';

	while (($sAttribute, $sValue) = each %$hFailoverFlags)
	{
		$sQuery = "UPDATE failover_flags SET $sAttribute='$sValue'";
		$iResult = $$hDb->do($sQuery);
		if (!$iResult)
		{
			setDiagnosticError(DIAG_SET_FAILOVER_FLAGS_DB_ERROR);
			$iReturnValue = 0;
			last;
		}
	}

	return $iReturnValue;
}

#####################################
# setSystemFailoverFlags
#####################################
sub setSystemFailoverFlags
{
	my $hDb = shift;
	my $hSystemFailoverFlags = shift;

	my $sQuery = '';
	my $hStatement = '';
	my $iResult = 0;
	my $iReturnValue = 1;
	my $sAttribute = '';
	my $sValue = '';

	while (($sAttribute, $sValue) = each %$hSystemFailoverFlags)
	{
		$sQuery = "UPDATE SystemFailoverFlags SET $sAttribute='$sValue'";
		$iResult = $$hDb->do($sQuery);
		if (!$iResult)
		{
			setDiagnosticError(DIAG_SET_FAILOVER_FLAGS_ATTRIBUTE_DB_ERROR);
			$iReturnValue = 0;
			last;
		}
	}

	return $iReturnValue;
}

#####################################
# getAvgPollTime
#
# NOTE: not exported
#####################################
sub getAvgPollTime
{
	my $hDb = shift;
	my $hProcess = shift;
	my $hAvgPollTime = shift;

	my $sQuery = '';
	my $hStatement = '';
	my $iResult = 0;
	my $iReturnValue = 1;
	my $iRowNum = 0;
	my $iSeconds = 0;
	my $sMessage = '';
	my @sRow = ();
	my @sRecords = ();
	my $iTotalSeconds = 0;

	# get 25 records so that there is one extra if polling is currently running
	# and the first record must be discarded
	$sQuery = "SELECT UNIX_TIMESTAMP(create_date), message FROM log WHERE origin='$$hProcess' ORDER BY id DESC LIMIT 0,25";
	$hStatement = $$hDb->prepare($sQuery);

	if (defined($hStatement))
	{
		$iResult = $hStatement->execute();

		if ($iResult)
		{
			# put records in array form processing
			while (($iSeconds, $sMessage) = $hStatement->fetchrow_array)
			{
				@sRow = [$iSeconds, $sMessage];
				push @sRecords, @sRow;
			}

			# check to see if first record is start message and remove
			if ($sRecords[0][1] =~ /start/)
			{
				shift @sRecords;
			}

			if ((scalar @sRecords) >= 24)
			{
				# process to find average
				for ($iRowNum = 0; $iRowNum < 24; $iRowNum += 2)
				{
					# make sure pairs are end and start records
					if (($sRecords[$iRowNum][1] =~ /end/) && ($sRecords[$iRowNum + 1][1] =~ /start/))
					{
						$iTotalSeconds += ($sRecords[$iRowNum][0] - $sRecords[$iRowNum + 1][0]);
					}
					else
					{
						setDiagnosticError(DIAG_RECORDS_NOT_PAIRS);
						$iReturnValue = DIAG_RECORDS_NOT_PAIRS->[0];
						$$hAvgPollTime = undef;
						last;
					}
				}

				# check to see if there is any seconds recorded
				if (!$iTotalSeconds)
				{
					setDiagnosticError(DIAG_NO_TOTAL_TIME);
					$iReturnValue = DIAG_NO_TOTAL_TIME->[0];
					$$hAvgPollTime = undef;
				}
			}
			else
			{
				setDiagnosticError(DIAG_POLL_SAMPLE_TOO_SMALL);
				$iReturnValue = DIAG_POLL_SAMPLE_TOO_SMALL->[0];
				$$hAvgPollTime = undef;
			}

			$hStatement->finish();

			$$hAvgPollTime = sprintf("%.2f", $iTotalSeconds/12);
		}
		else
		{
			setDiagnosticError(DIAG_AVG_POLL_TIME_DB_ERROR);
			$iReturnValue = 0;
			$$hAvgPollTime = undef;
		}
	}
	else
	{
		setDiagnosticError(DIAG_AVG_POLL_TIME_DB_ERROR);
		$iReturnValue = 0;
		$$hAvgPollTime = undef;
	}

	return $iReturnValue;
}

#####################################
# getLastPollTime
#
# NOTE: not exported
#####################################
sub getLastPollTime
{
	my $hDb = shift;
	my $hProcess = shift;
	my $hLastPollTime = shift;

	my $sQuery = '';
	my $hStatement = '';
	my $iResult = 0;
	my $iReturnValue = 1;
	my $iRowNum = 0;
	my $iSeconds = 0;
	my $sMessage = '';
	my @sRow = ();
	my @sRecords = ();

	# get 3 records so that there is one extra if polling is currently running
	# and the first record must be discarded
	$sQuery = "SELECT UNIX_TIMESTAMP(create_date), message FROM log WHERE origin='$$hProcess' ORDER BY id DESC LIMIT 0,3";
	$hStatement = $$hDb->prepare($sQuery);

	if (defined($hStatement))
	{
		$iResult = $hStatement->execute();

		if ($iResult)
		{
			# put records in array form processing
			while (($iSeconds, $sMessage) = $hStatement->fetchrow_array)
			{
				@sRow = [$iSeconds, $sMessage];
				push @sRecords, @sRow;
			}

			# check to see if first record is start message and remove
			if ($sRecords[0][1] =~ /start/)
			{
				shift @sRecords;
			}

			if ((scalar @sRecords) >= 2)
			{
				# make sure pairs are end and start records
				if (($sRecords[$iRowNum][1] =~ /end/) && ($sRecords[$iRowNum + 1][1] =~ /start/))
				{
					$$hLastPollTime = sprintf("%.2f", ($sRecords[$iRowNum][0] - $sRecords[$iRowNum + 1][0]));
				}
				else
				{
					setDiagnosticError(DIAG_RECORDS_NOT_PAIRS);
					$iReturnValue = DIAG_RECORDS_NOT_PAIRS->[0];
					$$hLastPollTime = undef;
				}
			}
			else
			{
				setDiagnosticError(DIAG_POLL_SAMPLE_TOO_SMALL);
				$iReturnValue = DIAG_POLL_SAMPLE_TOO_SMALL->[0];
				$$hLastPollTime = undef;
			}

			$hStatement->finish();

		}
		else
		{
			setDiagnosticError(DIAG_LAST_POLL_TIME_DB_ERROR);
			$iReturnValue = 0;
			$$hLastPollTime = undef;
		}
	}
	else
	{
		setDiagnosticError(DIAG_LAST_POLL_TIME_DB_ERROR);
		$iReturnValue = 0;
		$$hLastPollTime = undef;
	}

	return $iReturnValue;
}

#####################################
# getDiagnosticErrorNumber
#####################################
sub getDiagnosticErrorNumber
{
	my $hDiagnosticErrorNumber = shift;

	my $iReturnValue = 1;

	$iReturnValue = getLastErrorNumber($hDiagnosticErrorNumber);
	if (!$iReturnValue)
	{
		setDiagnosticError(DIAG_CANNOT_GET_LAST_ERROR_NUMBER);
		$iReturnValue = DIAG_CANNOT_GET_LAST_ERROR_NUMBER->[0];
	}

	return $iReturnValue;
}

#####################################
# getDiagnosticErrorString
#####################################
sub getDiagnosticErrorString
{
	my $hDiagnosticError = shift;

	my $iReturnValue = 1;

	$iReturnValue = getLastErrorString($hDiagnosticError);
	if (!$iReturnValue)
	{
		setDiagnosticError(DIAG_CANNOT_GET_LAST_ERROR_STRING);
		$iReturnValue = DIAG_CANNOT_GET_LAST_ERROR_STRING->[0];
	}

	return $iReturnValue;
}

#####################################
# setDiagnosticError
#####################################
sub setDiagnosticError
{
	my $hDiagnosticError = shift;

	my $iReturnValue = 1;

	$iReturnValue = setLastError($hDiagnosticError);
	if (!$iReturnValue)
	{
		setDiagnosticError(DIAG_CANNOT_SET_LAST_ERROR);
		$iReturnValue = DIAG_CANNOT_SET_LAST_ERROR->[0];
	}

	return $iReturnValue;

}

sub isCSLicenseActive
{
	my $dbh = shift;
	my $query = '';
	my $statement = '';
	my $result = 0;
	my $returnValue = 1;
	my $isLicenseStatus = 0;

	$query = "SELECT `value` FROM `CaseSentryConfig` WHERE `parm` = 'License is Active' AND `parm_group`='System'";

	$statement = $dbh->prepare($query);

	if (defined($statement)) {
		$result = $statement->execute();

		if ($result) {
			# put records in array form processing
			($isLicenseStatus) = $statement->fetchrow_array;

			$statement->finish();

			# if isLicenseStatus is undefined, a row was not returned. we will have to
			# assume that the license is valid
			$returnValue = (defined($isLicenseStatus) ? $isLicenseStatus : 1);
		}
		else {
			# an error has occured. we will have to assume that license is valid
			$returnValue = 1;
		}
	}
	else {
		# an error has occured. we will have to assume that license is valid
		$returnValue = 1;
	}

	return $returnValue;
}

sub isActive
{
    my $dbh       = shift;
    my $statement = 0;
    my $numrows   = 0;
    my $query = "SELECT `state` FROM `failover_flags` WHERE `state` = 'active' LIMIT 1";

    $dbh->prepare($query);
    $statement = $dbh->prepare($query);
    $statement->execute() or die "Cannot execute active CS query: " . $statement->errstr . "\n";
    $numrows = $statement->rows();

    return ($numrows == 1) ? 1: 0;
}

1;
