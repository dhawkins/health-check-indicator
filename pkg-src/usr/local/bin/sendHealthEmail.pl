#!/usr/bin/perl
#
# @todo add a brief one line description here...
#
# @todo add a detailed description of the file here...
#
# @version $Id: sendHealthEmail.pl 55143 2014-02-11 01:41:55Z bmladenov $
# @copyright 1999,2014, ShoreGroup, Inc.

use warnings;
use strict;
use Getopt::Long;

use lib "/usr/lib/health-indicators/perl";

use sendHealthEmail;

my $DEBUG = 0;
my $help = 0;
my $systemType = "";
my $testType = "";
my $emailAddress = "";
my $entity = `/bin/cat /etc/hostname`;
$entity =~ s/\n//g;
my $fromAddress = "";

# Try to auto populate this.
# If it dies here, it was going to die on health checks anyways so not a problem.
open CONFIG, "<", "/etc/health-indicators/HealthCheckConfig.tsv" or die $!;
my @lines = <CONFIG>;
close CONFIG or die $!;

if ( $lines[0] =~ m/^SystemType/o ) {
	$systemType = shift @lines;
	$systemType =~ s/(SystemType=|\n)//g;
}

GetOptions(
	"debug" => \$DEBUG,
	"help" => \$help,
	"system-type=s" => \$systemType,
	"test-type=s" => \$testType,
	"email-address=s" => \$emailAddress,
	"from-address=s" => \$fromAddress,
	"entity=s" => \$entity
);

if ($DEBUG == 1) {
	print "Email: $emailAddress\n";
	print "From Address: $fromAddress\n";
	print "Test Type: $testType\n";
	print "System Type: $systemType\n";
	print "Entity: " . $entity . "\n";
}

if ($help == 1 || $systemType eq "" || $testType eq "" || $emailAddress eq "" || $entity eq "" || $fromAddress eq "") {
	print "Flags: \n";
	print "--help\t\t\tShow this message\n";
	print "--test-type\t\tSet the test type to use.\n";
	print "\tAllowed options:\n";
	print "\t<system | backups | database | queues | application | processes>\n";
	print "--email-address\t\tWho to send the health email to.\n";
	print "--from-address\t\tWho to send the email as.\n";
	print "--entity\t\t\tThe object to report health status about.\n";
	print "--system-type\t\tSet the system type used when reading configuration settings.\n";
	print "\tDefault options:\n";
	print "\t< Web | Poller | Hot Standby | 2U | Report | Fieldwatch>\n";
	exit(0);
}

$testType = lc($testType);

if ($testType eq "system"
|| $testType eq "backups"
|| $testType eq "replication"
|| $testType eq "queues" 
|| $testType eq "application"
|| $testType eq "database" 
|| $testType eq "processes") {
}
else {
	print "--test-type\t\tSet the test type to use.\n";
	print "\tAllowed options:\n";
	print "\t<system | backups | database | queues | application | processes>\n";
	exit(0);
}

my $lockFile = "/tmp/healthwarn_" . $testType . ".lock";

my $returnCode;

my $healthCheckCommand = "/usr/bin/sudo /usr/local/bin/health_checks.pl --test-group=" . $testType;

$returnCode = system($healthCheckCommand . " --snmp-output=test");
$returnCode = $returnCode >> 8;

# Set to results mode to get rid of the color codes.
my $output = `$healthCheckCommand --snmp-output=results`;
$output =~ s/'/'\\''/g;

# determine if email should be sent
my $status = sendEmail($returnCode,$lockFile);
if ($status ne 'pass') {
    my $sendEmailCommand = "/bin/echo '" . $output . "' | /usr/bin/mail -s 'Health:" . $entity . ":" . $testType . ":" . $status . "' '" . $emailAddress . "' -- -f '" . $fromAddress . "'";
    print $sendEmailCommand . "\n" if $DEBUG;
    `$sendEmailCommand`;
}

exit(0);
