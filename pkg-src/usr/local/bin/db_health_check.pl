#!/usr/bin/perl

# $
# @copyright 1999,2016, ShoreGroup, Inc.

# Script checks to see if the MySQL database is up and available for connections. If it is available, it will
# report on the number of active connections to the database. If the number of connections are over the 
# threshold, it will report an error.  The output is sent through stdout in the format used by CCS / CMAP for
# the health checks. See print_usage function for options and arguments. Case CST-38225 (R.Dean) 


use warnings;
use strict;
use DBI;
use Getopt::Long;
use Term::ANSIColor qw(:constants);

use constant EXIT_GOOD => 2;
use constant EXIT_BAD => 1;


our $hasFailed = 0;

# Assign global variables
my $DEBUG;
my $help;
my $active;
my $group;
my $group_arg;
my $output;
my $snmp = "normal";
my $system_type;
my $db_ip;
my $db_port;
my $db_user;
my $db_pass;
my $threshold_pct;
my $pass_string;
my $err_flag = 0;
my $err_chk;
my $name = "MySql Number of Connections";
my $default_max_connections = 151;
my $defaultThreshold_pct = .75;
my $status = "";
my $notes;

my %snmps = (
        "results" => 0,
        "test" => 0
);


GetOptions( "debug"         	=> \$DEBUG,
            "help"          	=> \$help,
            "ip=s"     		=> \$db_ip,
            "port=s"    	=> \$db_port,
            "user=s"    	=> \$db_user,
            "pass=s"     	=> \$db_pass,
            "threshold=s"	=> \$threshold_pct,
            "error_check"       => \$err_chk,
            "snmp-output=s" 	=> \$output);

# Check arguments 
if ( $help ) {
	&print_usage;
}

if ( $output ) {
        if ( exists $snmps{ $output } ) {
                $snmp = $output;
        }
        else {
                print "Unknown output type: " . $output . "\n\n";
                &print_usage;
        }
}

if ( ! $db_ip ) {
   $db_ip = "localhost"   
}

if ( ! $db_port ) {
   $db_port = 3306   
}

if ( ! $db_user ) {
   $db_user = "root"   
}

if ( ! $db_pass ) {
   $pass_string = "";
}
else { 
   $pass_string = sprintf("-p\'%s\'",$db_pass); 
}

if ( ! $threshold_pct ) {
   $threshold_pct = $defaultThreshold_pct;
}
 
$group = "db_threshold";
my @group_status;


# Ensure DB is up and accessable
  my $db_status = `/usr/bin/mysqladmin -u $db_user $pass_string -h $db_ip -P $db_port  status 2>&1`;
  if (  $db_status =~ /Too many connections/ ) {
       $status = "FAIL"; 
       $name = "MySql Connection";
       $notes = sprintf("Over the number of active database connections allowed to %s",$db_ip); 
       $err_flag = 1;
  }
  elsif ( $db_status =~ /connect to server at .* failed/ ) {
       $status = "FAIL"; 
       $name = "MySql Connection";
       $notes = sprintf("Failed to connect to database at %s",$db_ip); 
       $err_flag = 1;
  }
  elsif ( ! ($db_status =~ /^Uptime/) ) {
       $status = "FAIL"; 
       $name = "MySql Connection";
       $notes = "Output of mysqladmin command is not in format expected"; 
       $err_flag = 1;
  } 
  elsif (! $err_chk)  {
        #Determine number of current connections to database and max number of connections.      
        my @outputArray = split(/ /,$db_status);
        my $curr_connections = $outputArray[4]; 
        my $max_connections = `/usr/bin/mysql  -u $db_user $pass_string -h $db_ip -P $db_port -e" SHOW GLOBAL VARIABLES LIKE 'max_connections' " | 
                               /usr/bin/awk ' \$1 == "max_connections" { print \$2 } ' `;
        # use mysql default if query does not return a proper value
        if ( $max_connections < 0 || ! $max_connections ) {
              $max_connections = $default_max_connections; 
              printf("Warning: max_connections not returning a proper value.  Return was: %s",$max_connections) if $DEBUG;

        }
        my $current_percent = $curr_connections / $max_connections;
        if (  $current_percent < $threshold_pct ) { 
             $status = "PASS" ;
             $notes = sprintf("Number of current database connections of %d, is below Threshold of %d. Max Connections is %d",
                             $curr_connections,$threshold_pct *  $max_connections, $max_connections) ;

        }
        else {
            $status = "FAIL" ;
            $notes = sprintf("Number of current database connections of %d, is above Threshold of %d, Max connections is %d",
                            $curr_connections,$threshold_pct * $max_connections, $max_connections) ;
            $err_flag = 1;
        }
  } 


# Print all results if $err_chk is not set. If $err_chk is set only print if db was not accessable.
if (! $err_chk  || $status eq "FAIL" ) {
	printf "================================= %s =================================%s", uc($group), "\n" unless $snmp eq "test";

         &PrintStatus($group,$name,$status,$notes);



	if ( $err_flag ) {
		print "================================= SUMMARY =================================\n";
		print "HealthCheck Results: ";
		print RED, "CHECK!  A FAILURE CONDITION EXISTS.  PLEASE INVESTIGATE.\n", RESET;
	} 
	else {
		print "================================= SUMMARY =================================\n";
		print "HealthCheck Results: ";
		print GREEN, "OK.\n", RESET;
	}
}

print "\n----Info----\n"
. "group: " . $group . "\n"
. "output: " . $snmp . "\n"
. "Done\n" if $DEBUG;

# NOTE: If we review how the tests are set up in snmpPluginDef, an Engineer will find
# that the Normal (good) value is 2 and Critical values are set to 1 or unknown.
if ($hasFailed) {
	exit EXIT_BAD;
}
else {
	exit EXIT_GOOD;
}

############################## End Main ##############################
sub print_usage {
 die "Usage: ./health_checks.pl [threshold=<.0 to .99>] [--ip=<db ip address>] [--port=<db port>] [--user=<db username>] [--pass=<db password>] [--snmp-output='OUTPUT_OPTION']"
	. "\n"
	. "\n"
	. "\t--help                           Display this message\n"
	. "\t--debug                          Enable debugging\n"
	. "\t--error_check                    Script will only check if the database is not accessable. If it is not it will print the message in CSS/CMAP\n"
        . "\t--threshold		      Percentage to warn of exceeding connections. Default is .75.\n"
        . "\t--ip		      	      IP address of database server. Default is: localhost.\n"
        . "\t--port		      	      Listener on database server. Default is: 3306.\n"
        . "\t--user		      	      Username to log into the database server. Default is: root.\n"
        . "\t--pass		      	      Password of database server. Default is: NULL.\n"
	. "\t--snmp-output='OUTPUT_OPTION'    Display output in the form that SNMP uses for CCS/CMAP. Requires use of '--test-group'.\n"
	. "\t\tresults\n"
	. "\t\ttest\n";
}
sub PrintStatus ($$$$) {
        my ($group,$name,$status,$notes) = @_;

        if ( $status eq "FAIL" && $snmp eq "test" ) {
                print "CHECK\n";
                exit EXIT_BAD;
        }
        else {
                push @group_status, "$status";
        }

        if ( $snmp eq "results" ) {
                print $name . ": " . $status . "\n";
                print "\t" . $notes . "\n";
                if ($status eq "FAIL") {
                        $hasFailed = 1;
                }
        }
        elsif ( !$output ) {
                print GREEN, $name . ": " . "$status\n", RESET if $status eq "PASS";
                print YELLOW, $name . ": " . "$status\n", RESET if $status eq "WARN";
                print GREEN, $name . ": " . "$status\n", RESET if $status eq "N/A";
                print RED,   $name . ": " . "$status\n", RESET if $status eq "FAIL";
                print BLUE,  $name . ": " . "$status\n", RESET if $status eq "UNKNOWN";
                if ($status eq "FAIL") {
                        $hasFailed = 1;
                }
                print "\t" . $notes . "\n";
        }
        elsif ( $snmp eq "test" ) {
                #my $tests_to_run = keys %{$HealthConfig{ $group }};
                my $tests_to_run = 1;
                my $tests_ran = @group_status;

                if ( $tests_ran == $tests_to_run ) {
                        my @check = grep(/FAIL/, @group_status);
                        print "OK\n" if !@check;
                        print "CHECK\n" if @check;
                        if (@check) {
                                $hasFailed = 1;
                        }
                }
        }
}

