#!/usr/bin/perl

# @version $Id: health_checks.pl 70748 2014-12-12 18:59:27Z bblackmoor $
# @copyright 1999,2016, ShoreGroup, Inc.

# Following BEGIN statement is a patch to insure the script does not crash if the database is not available 
# or a connection cannot be made to it. The script called "db_health_check.pl", will ensure the db is either
# available, where the script will move on to compile or will send an error message to stdout and this script
# will exit. 

BEGIN {
	system("/usr/local/bin/db_health_check.pl --error_check");
	my $retval =  $? >> 8;
	if ($retval != 2 ) {
		#printf("Val = |%s|\n", $retval);
		exit(1);
	}
}

use warnings;
use strict;
use DBI;
use Getopt::Long;
use Term::ANSIColor qw(:constants);
use lib "/usr/lib/health-indicators/perl";
use ConnectVars;
use CS_Diagnostics qw( isActive );

use constant EXIT_GOOD => 2;
use constant EXIT_BAD => 1;

sub patchLevel();

my $hDb = '';

if ($hDb = getConnection('Main')) {
	# do nothing, everything is okay

}
else {
	# was: die "Exiting - Main database connection failed: $DBI::errstr";
	print "CHECK\n";
	exit EXIT_BAD;
}

our $hasFailed = 0;

# Assign global variables
my $DEBUG;
my $help;
my $active;
my $group;
my $group_arg;
my $output;
my $snmp = "normal";
my $system_type;

my %snmps = (
	"results" => 0,
	"test" => 0
);
my %groups = (
	"all" => 0,
	"application" => 0,
	"backups" => 0,
	"database" => 0,
	"processes" => 0,
	"queues" => 0,
	"system" => 0
);

if (isActive($hDb)) {
 $active = 1;
}

GetOptions("debug"      => \$DEBUG,
		"help"          => \$help,
		"test-group=s"  => \$group_arg,
		"snmp-output=s" => \$output);

# Check arguments and act
if ( $help ) {
	&print_usage;
}
elsif ( ( $output ) && ( !$group_arg ) ) {
	&print_usage;
}
elsif ( ( $output ) && ( $group_arg ) ) {
	if ( exists $snmps{ $output } ) {
		$snmp = $output;
	}
	else {
		print "Unknown output type: " . $output . "\n\n";
		&print_usage;
	}
	if ( exists $groups{ $group_arg } ) {
		$group = $group_arg;
	}
	else {
		print "Unknown group option: " . $group_arg . "\n\n";
		&print_usage;
	}
}
elsif ( $group_arg ) {
	if ( exists $groups{ $group_arg } ) {
		$group = $group_arg;
	}
	else {
		print "Unknown group option: " . $group_arg . "\n\n";
		&print_usage;
	}
}
else {
	$group = "all";
}

# Load health check config file
my %HealthConfig;
&LoadConfig($group);

# Run the health checks
my @group_status;
my @running_processes = `/bin/ps auwx`;
if ($DEBUG) {
	print "Running processes: \n";
	for my $process (@running_processes) {
		print $process . "\n";
	}
}

print "System Type: " . $system_type . "\n" if !$output;

foreach my $group ( sort keys %HealthConfig ) {
	printf "================================= %s =================================%s", uc($group), "\n" unless $snmp eq "test";
	foreach my $name ( sort keys %{$HealthConfig{ $group }} ) {
		foreach my $function ( sort keys %{$HealthConfig{ $group }{ $name }} ) {
			foreach my $param1 ( sort keys %{$HealthConfig{ $group }{ $name }{ $function }} ) {
				foreach my $param2 ( sort keys %{$HealthConfig{ $group }{ $name }{ $function }{ $param1 }} ) {
					foreach my $param3 ( sort keys %{$HealthConfig{ $group }{ $name }{ $function }{ $param1 }{ $param2 }} ) {
						&HealthCheck($group,$name,$function,$param1,$param2,$param3);
					}
				}
			}
		}
	}
}

if ( $group eq "all" ) {
 my @check = grep(/FAIL/, @group_status);
	if ( @check ) {
	print "================================= SUMMARY =================================\n";
	print "HealthCheck Results: ";
	print RED, "CHECK!  A FAILURE CONDITION EXISTS.  PLEASE INVESTIGATE.\n", RESET;
	print "Patch Level: " . patchLevel() . "\n";
	print "System Architecture: " . $system_type . "\n";
	} 
	else {
	print "================================= SUMMARY =================================\n";
	print "HealthCheck Results: ";
	print GREEN, "OK.\n", RESET;
	print "Patch Level: " . patchLevel() . "\n";
	print "System Architecture: " . $system_type . "\n";
	}
}

print "\n----Info----\n"
. "group: " . $group . "\n"
. "output: " . $snmp . "\n"
. "Done\n" if $DEBUG;

# NOTE: If we review how the tests are set up in snmpPluginDef, an Engineer will find
# that the Normal (good) value is 2 and Critical values are set to 1 or unknown.
if ($hasFailed) {
	exit EXIT_BAD;
}
else {
	exit EXIT_GOOD;
}

############################## End Main ##############################
sub print_usage {
	die "Usage: ./health_checks.pl [--test-group='GROUP_NAME'|(--snmp-output='OUTPUT_OPTION' --test-group='GROUP_NAME')]"
		. "\n"
		. "\n"
		. "\t--help                           Display this message\n"
		. "\t--debug                          Enable debugging\n"
		. "\t--test-group='GROUP_NAME'        Specify the test group you which to run\n"
		. "\t\tall\n"
		. "\t\tapplication\n"
		. "\t\tbackups\n"
		. "\t\tdatabase\n"
		. "\t\tprocesses\n"
		. "\t\tqueues\n"
		. "\t\tsystem\n"
		. "\t--snmp-output='OUTPUT_OPTION'    Display output in the form that SNMP uses for CCS/CMAP. Requires use of '--test-group'.\n"
		. "\t\tresults\n"
		. "\t\ttest\n";
}

sub LoadConfig ($) {
	my $group_to_use = shift;

	open CONFIG, "<", "/etc/health-indicators/HealthCheckConfig.tsv" or die $!;
	my @lines = <CONFIG>;
	close CONFIG or die $!;

	if ( $lines[0] =~ m/^SystemType/o ) {
		$system_type = shift @lines;
		$system_type =~ s/(SystemType=|\n)//g;
	}
	else {
		die "Unable to determine system type from Config File '/etc/health-indicators/HealthCheckConfig.tsv'\n\n"
		. "Please build config file with '/usr/local/bin/WriteHealthCheckCFG.pl'\n";
	}

	foreach my $line ( @lines ) {
		chomp($line);
		my ($group,$name,$func,$param1,$param2,$param3) = split("\t", $line);

		if ( $group_to_use eq "all" ) {
			$HealthConfig{ $group }{ $name }{ $func }{ $param1 }{ $param2 }{ $param3 } = 0;
		}
		else {
			$HealthConfig{ $group }{ $name }{ $func }{ $param1 }{ $param2 }{ $param3 } = 0 if $group eq $group_to_use;
		}
	}
}

sub HealthCheck ($$$$$$) {
	my ( $group, $name, $function, $param1, $param2, $param3 ) = @_;

	if ( lc($group) eq 'all' ) {
		&application_check($name,$function,$param1,$param2,$param3);
		&backups_check($name,$function,$param1,$param2,$param3);
		&processes_check($name,$function,$param1,$param2,$param3);
		&queues_check($name,$function,$param1,$param2,$param3);
		&database_check($name,$function,$param1,$param2,$param3);
		&system_check($name,$function,$param1,$param2,$param3);
	}
	elsif ( $group =~ m/application/io ) {
		&application_check($name,$function,$param1,$param2,$param3);
	}
	elsif ( $group =~ m/backups/io ) {
		&backups_check($name,$function,$param1,$param2,$param3);
	}
	elsif ( $group =~ m/processes/io ) {
		&processes_check($name,$function,$param1,$param2,$param3);
	}
	elsif ( $group =~ m/queues/io ) {
		&queues_check($name,$function,$param1,$param2,$param3);
	}
	elsif ( $group =~ m/(replication|database)/io ) {
		&database_check($name,$function,$param1,$param2,$param3);
	}
	elsif ( $group =~ m/system/io ) {
		&system_check($name,$function,$param1,$param2,$param3);
	}
	else {
		warn "Unknown test-group: " . $group . "\n";
	}
}

sub fetchHealthCheckValue($) {
	my $healthCheck = shift;
	my $sql = "SELECT value FROM sys_health WHERE health_check = '" . $healthCheck . "'";
	my $query = $hDb->prepare( $sql );
	$query->execute();
	print BLUE . $sql . RESET . "\n" if $DEBUG;
	my @results = $query->fetchrow_array();
	return $results[0];
}

sub patchLevel() {
	my $sql = "SELECT value FROM sys_param WHERE param='PatchLevel'";
	my $query = $hDb->prepare( $sql );
	$query->execute();
	print BLUE . $sql . RESET . "\n" if $DEBUG;
	my @results = $query->fetchrow_array();
	return $results[0];
}

sub application_check ($$$$$) {
	my ($name,$function,$param1,$param2,$param3) = @_;
	my $status = "UNKNOWN";
	my $notes = "";

	if ( $function eq "checkAutoOpen" ) {
                my $sql = "SELECT COUNT(*) FROM `Case_Management`.`case_update` WHERE `is_autocase` = 1 AND `create_time` >= UNIX_TIMESTAMP() - (60 * " . $param1 .")";
		my $query = $hDb->prepare( $sql );
		$query->execute();
		print BLUE . $sql . RESET . "\n" if $DEBUG;
		my @results = $query->fetchrow_array();
		if (@results) {
			$status = "PASS" if $results[0] >= 1;
			$status = "FAIL" if $results[0] < 1;
			$status = "N/A" if !$active;
			$notes = "Found " . $results[0] . " auto generated records opened or updated in the last " . $param1 . " minutes";
			$notes = "N/A (inActive): " . $notes if !$active;
		}
		else {
			$status = "FAIL";
			$notes = $sql . " failed to return data.";
		}
	}
	elsif ( $function eq "tableRecords" ) {
		my $sql = "SELECT COUNT(*) FROM $param1";
		my $query = $hDb->prepare( $sql );
		$query->execute();
		print BLUE . $sql . RESET . "\n" if $DEBUG;
		my @results = $query->fetchrow_array();
		$status = "PASS" if $results[0] < $param2;
		$status = "FAIL" if $results[0] >= $param2;
		$status = "N/A" if !$active;
		$notes = "Found $results[0] rows in $param1, the threshold is $param2";
		$notes = "N/A (inActive): " . $notes if !$active;
	}
	elsif ( $function eq "checkLoginPage" ) {
		my $login_ip;
		my $login_url;
		if ( $param1 ) {
			$login_ip = $param1;
		}
		else {
			#It can't parse localhost for some reason...
			$login_ip = '127.0.0.1';
		}
		if ( $param2 ) {
			$login_url = $param2;
		}
		else {
			$login_url = '/yii_entry.php?r=auth/index';
		}
		my $web_status = `/usr/lib/nagios/plugins/check_http -I $login_ip -S -u $login_url  2>/dev/null`;
		print BLUE . "/usr/lib/nagios/plugins/check_http -I " . $login_ip . " -S -u " . $login_url . " 2>/dev/null" . RESET . "\n" if $DEBUG;
		$status = "PASS" if $web_status =~ m/http ok:/io;
		$status = "FAIL" if $web_status !~ m/http ok:/io;
		$notes = "Checking access to: /usr/lib/nagios/plugins/check_http -I " . $login_ip . "-S -u " .$login_url;
	}
	elsif ( $function eq "PerfPoll" ) {
		my $rrd_last = `/usr/bin/rrdtool last /var/lib/rrd/$param1`;
		print BLUE . "/usr/bin/rrdtool last /var/lib/rrd/" . $param1 . RESET . "\n" if $DEBUG;
		chomp($rrd_last);
		my $cur_time = scalar time;

		if ( ($cur_time - $rrd_last) < $param2 ) {
		$status = "PASS";
		}
		else {
		$status = "FAIL";
		}
		$notes = "Last update time for /var/lib/rrd/" . $param1 . " is " . $rrd_last ." (" . ($cur_time - $rrd_last) . " seconds ago), Threshold is " . $param2;
	}
	elsif ( $function eq "dbTime" ) {
		# Connect to the snmptt database connection if needed
		if ($param1 eq "snmptt") {
			$hDb = getConnection('snmptt') or die "Exiting - snmptt database connection failed: $DBI::errstr";
		}

		my $sql = "SELECT IFNULL((UNIX_TIMESTAMP(NOW()) - MAX(" . $param2 . ")), 4081) FROM `" . $param1 . "`";
		my $query = $hDb->prepare( $sql );
		$query->execute();
		print BLUE . $sql . RESET . "\n" if $DEBUG;
		my @results = $query->fetchrow_array() || ("NULL");

		$status = "PASS" if $results[0] <= $param3;
		$status = "FAIL" if $results[0] > $param3;
		$status = "N/A" if !$active;
		$notes = "Last entry logged to $param1 was $results[0] seconds ago" if $results[0] != 4081;
		$notes = "No entries found in $param1" if $results[0] == 4081;
		$notes = "N/A (inActive): " . $notes if !$active;

		# Reconnect to the Main database connection if it was changed
		if ($param1 eq "snmptt") {
			$hDb = getConnection('Main') or die "Exiting - Main database connection failed: $DBI::errstr";
		}
	}
	elsif ( $function eq "checkTimeStamp" ) {
		my $sql = sprintf('SELECT CEIL(((UNIX_TIMESTAMP(NOW())) - (UNIX_TIMESTAMP(`value`)))/60) AS `timeDifference` FROM `%s` where param = \'%s\'', $param1, $param2);
		my $query = $hDb->prepare( $sql );
		$query->execute();
		print BLUE . $sql . RESET . "\n" if $DEBUG;
		my @results = $query->fetchrow_array();
		if (@results) {
			if ($results[0] < $param3) {
				$status = "PASS";
				$notes = sprintf("Time of %d minutes is within the threshold of %d minutes.",$results[0],$param3);
			}
			else {
				$status = "FAIL";
				$notes = sprintf("Time of %d minutes exceeded the threshold of %d minutes.",$results[0],$param3);
			}
		}
		else {
			$status = "FAIL";
			$notes = $sql . " failed to return data.";
		}

		$status = "N/A" if !$active;
		$notes = "N/A (inActive): " . $notes if !$active;
	}
	elsif ( $function eq "RedundancyState" ) {
		my $sql = "SELECT `identity`, `state`, `health`, IF( ( `identity` = 'primary' AND `state` = 'inactive' ) OR ( `identity` = 'secondary' AND `state` = 'active' ) OR ( `health` = 'bad' ), 'CHECK!', 'OK' ) FROM `failover_flags`";
		my $query = $hDb->prepare( $sql );
		$query->execute();
		print BLUE . $sql . RESET . "\n" if $DEBUG;
		my @results = $query->fetchrow_array();
		if (@results) {
			$status = "PASS" if $results[3] eq 'OK';
			$status = "FAIL" if $results[3] eq 'CHECK!';
			$notes = "Redundancy Status: (" . $results[0] . ", " . $results[1] . ", " . $results[2] . ")";
		}
	}
	elsif ( $function eq "ConsolidatedPoller" ) {
		if (!$active) {
			$status = "N/A";
			$notes = "N/A (inActive): Ports are inactive";
		} else {
			my $out = `/var/www/CaseSentry/bin/testConsolidatedPollerPorts.sh 2>&1` ;
			chomp($out);
			if($out =~ /OK/){
				$status = "PASS";
			}
			else {
				$status = "FAIL";
				$notes = 'Try running /var/www/CaseSentry/bin/find-long-running-pollers.pl or Restart Consolidated Poller: https://cs-team/atg-docs/?p=4142';
			}
		}
	}
	elsif ( $function eq "dbTimeIntegration" ) {
		my $sql = "SELECT value FROM CaseSentryConfig where parm ='isIntegration'";
		my $query = $hDb->prepare( $sql );
		$query->execute();
		my @results = $query->fetchrow_array() || 0;
		if ( $results[ 0 ] == 0 ) {
			$status = "PASS";
			$notes  = "There is no integration";
		}
		else {
			$sql = "SELECT value FROM CaseSentryConfig where parm ='execOnCaseUpdate'";
			$query = $hDb->prepare( $sql );
			$query->execute();
			@results = $query->fetchrow_array() || ( '' );
			if ( $results[ 0 ] eq '' ) {
				$status = "FAIL";
				$notes  = "Integration is NOT ON!!";
			}
			else {
				$sql = "SELECT IFNULL((UNIX_TIMESTAMP(NOW()) -  UNIX_TIMESTAMP(max(`last_sent`))), 4081) FROM integrations_ticket";
				$query = $hDb->prepare( $sql );
				$query->execute();
				print BLUE . $sql . RESET . "\n" if $DEBUG;
				@results = $query->fetchrow_array() || ( "NULL" );
				if ( $results[ 0 ] > $param3 ) {
					$status = "FAIL";
					$notes  = "Last entry logged was $results[0] seconds ago"
					if $results[ 0 ] != 4081;
					$notes = "There is NOTHING in integrations_ticket"
					if $results[ 0 ] == 4081;
				}
				else {
					my $sql1 = "select count(*) from integrations_outbound_queue";
					$query = $hDb->prepare( $sql1 );
					$query->execute();
					print BLUE . $sql1 . RESET . "\n" if $DEBUG;
					@results = $query->fetchrow_array() || ( "NULL" );
					my $Intcount = $results[ 0 ];
					my $sql = "select (unix_timestamp(last_attempt_date) - unix_timestamp(first_attempt_date)) from integrations_outbound_queue order by first_attempt_date asc limit 1";
					$query = $hDb->prepare( $sql );
					$query->execute();
					print BLUE . $sql . RESET . "\n" if $DEBUG;
					@results = $query->fetchrow_array() || ( "NULL" );
					my $OQresult = $results[ 0 ];
					$OQresult = 0 if $results[ 0 ] eq "NULL";

					if ( ( $OQresult > $param3 ) && $Intcount > 0 ) {
						$status = "FAIL";
						$notes = "There is a ticket in Integrations_outbound_queue that has been trying to send for $results[0] seconds";
					}
					else {
						$status = "PASS";
						$notes  = "Integration is functioning normally";
					}
				}
			}
		}
	}
    elsif ( $function eq "checkLDAP" ) {
        my $sql   = "select count(*) from ldap_servers where active =1";
            my $query = $hDb->prepare( $sql );
            $query->execute();
            print BLUE . $sql . RESET . "\n" if $DEBUG;
            my @results = $query->fetchrow_array();
            if ($results[0] == 0 || !$active )
            {
                    $status = "PASS";
                    $notes = "There is no LDAP server or this is a secondary";
            }
            else
            {
                    if (`/var/www/CaseSentry/protected/yii_console.php usercli ldapBind --id=1` =~ m/Bind Success/)
                    {
                            $status = "PASS";
                            $notes = "LDAP synthetic login success";
                    }
                    else
                    {
                            $status = "FAIL";
                            $notes = "LDAP synthetic login failed";
                    }
            }
	}
	elsif ( $function eq "soapPullStatus" ) {
		my $lastComplete = `tail -n1 /tmp/fw_mail_pull_last_completed`;
		my $now = time;
		my $delta = $now - $lastComplete;

		if ( $delta > $param1 ) {
			$status = "FAIL";
			$notes = "The last reported successful SOAP pull was more than 10 minutes ago";
		}
		else {
			$status = "PASS";
			$notes = "The last reported succesful SOAP pull was less than 10 minutes ago ($delta seconds ago)";
		}
	}        

	&PrintStatus($group,$name,$status,$notes);
}

sub backups_check ($$$$$) {
	my ($name,$function,$param1,$param2,$param3) = @_;
	my $status = "UNKNOWN";
	my $notes = "";

	if ( $function eq "BackupCheck" ) {
		my $sql = "SELECT DATEDIFF(NOW(), `value`)"
		. " FROM `" . $param1 . "`"
		. " WHERE `health_check` = '" . $param2 . "'";
		my $query = $hDb->prepare( $sql );
		$query->execute();
		print BLUE . $sql . RESET . "\n" if $DEBUG;
		my @results = $query->fetchrow_array();
		if (@results) {
			$status = "PASS" if $results[0] <= $param3;
			$status = "FAIL" if $results[0] > $param3;
			$notes = "Days between now and value in " . $param1 . " for param " . $param2 . " is " . $results[0] . " days, The threshold is " . $param3 . " days";
		}
		else {
			$status = "FAIL";
			$notes = $sql . " failed to return data.";
		}
	}

	&PrintStatus($group,$name,$status,$notes);
}

sub processes_check ($$$$$) {
	my ($name,$function,$param1,$param2,$param3) = @_;
	my $status = "UNKNOWN";
	my $notes = "";

	if ( $function eq "checkPs" ) {
		my @process_to_check = `ps aux | grep '$param2' | grep -v grep`;
		$status = "PASS" if @process_to_check;
		$status = "FAIL" if !@process_to_check;
		$notes = "Process \"$param1\" is running" if $status eq "PASS";
		$notes = "Process \"$param1\" is not running" if $status eq "FAIL";
	}
	elsif ( $function eq "checkTcp" ) {
		my $tcp_status = `/usr/lib/nagios/plugins/check_tcp -H $param2 -p $param3`;
		print BLUE . "/usr/lib/nagios/plugins/check_tcp -H " . $param2 . " -p " . $param3 . RESET . "\n" if $DEBUG;
		$status = "PASS" if $tcp_status =~ m/TCP OK/io;
		$status = "FAIL" if  $tcp_status !~ m/TCP OK/io;
		$notes = "TCP Port " . $param3 . " is responding on host " . $param2;
	}
	&PrintStatus($group,$name,$status,$notes);
}

sub queues_check ($$$$$) {
	my ($name,$function,$param1,$param2,$param3) = @_;
	my $status = "UNKNOWN";
	my $notes = "";

	if ( ( $function eq "queueCheck" ) && ( $param1 eq "db" ) ) {
		my $sql = "SELECT COUNT(*) FROM `" . $param3 . "`";
		my $query = $hDb->prepare( $sql );
		$query->execute();
		print BLUE . $sql . RESET . "\n" if $DEBUG;
		my @results = $query->fetchrow_array();
		if (@results) {
			$status = "PASS" if $results[0] <= $param2;
			$status = "FAIL" if $results[0] > $param2;
			$status = "N/A" if !$active;
			$notes = "Found " . $results[0] . " records in " . $param3 . ", the threshold is " . $param2;
			$notes = "N/A (inActive): " . $notes if !$active;
		}
		else {
			$status = "FAIL";
			$notes = $sql . " failed to return data.";
		}
	}
	elsif ( ( $function eq "queueCheck" ) && ( $param1 eq "directory" ) ) {
		my $files_queued = `/usr/bin/tree $param3 | /usr/bin/tail -n 1 | /usr/bin/awk '{print \$3}'`;
		print BLUE . "/usr/bin/tree " . $param3 . " | /usr/bin/tail -n 1 | /usr/bin/awk '{print \$3}'" . RESET . "\n" if $DEBUG;
		chomp($files_queued);

		$status = "PASS" if $files_queued <= $param2;
		$status = "FAIL" if $files_queued > $param2;
		$notes = "Found " . $files_queued . " files in " . $param3 . ", the threshold is " . $param2;
	}
	elsif ( ( $function eq "queueCheck" ) && ( $name eq "ondemand_object_queue" ) ) {
		my $sql = "SELECT COUNT(*) FROM `" . $param3 . "` WHERE last_update < DATE_SUB(NOW(), INTERVAL " . $param2 . " MINUTE)";
		my $query = $hDb->prepare( $sql );
		$query->execute();
		print BLUE . $sql . RESET . "\n" if $DEBUG;
		my @results = $query->fetchrow_array();

		$status = "PASS" if $results[0] <= $param2;
		$status = "FAIL" if $results[0] > $param2;
		$status = "N/A" if !$active;
		$notes = "Found " . $results[0] . " records in " . $param3 . ", the threshold is " . $param2;
		$notes = "N/A (inActive): " . $notes if !$active;
	}
	&PrintStatus($group,$name,$status,$notes);
}

sub database_check ($$$$$) {
	my ($name,$function,$param1,$param2,$param3) = @_;
	my $status = "UNKNOWN";
	my $notes = "";

	if ( $function eq "ReplicationCheck" ) {
		my $sql = "SELECT (UNIX_TIMESTAMP(NOW()) - value)"
		. " FROM `" . $param1 ."`"
		. " WHERE param = '" . $param2 . "'";

		my $query = $hDb->prepare( $sql );
		$query->execute();
		print BLUE . $sql . RESET . "\n" if $DEBUG;
		my @results = $query->fetchrow_array();
		if (@results) {
			$status = "PASS" if $results[0] <= $param3;
			$status = "FAIL" if $results[0] > $param3;
			$status = "N/A" if $active;
			$notes = "Time difference between now and value in " . $param1 . " for param " . $param2 . " is " . $results[0] . " seconds";
			$notes = "N/A (Active): " . $notes if $active;
		}
		else {
			if ($active) {
				$status = "PASS";
				$notes = "Test N/A on active CaseSentry.";
			}
			else {
				$status = "FAIL";
				$notes = $sql . " failed to return data.";
			}
		}
	}
  

	if ( $function eq "checkDbConnections" ) {
		my $defaultThreshold_pct = .75;
		my $errFlag = 0;
		my $max_connections = "";
		my $curr_connections = "";
		my $threshold_pct;
		my $sql;
		my $query;
		my $results;
		my @results;

		$sql = "SELECT `value` FROM CaseSentry.sys_health WHERE `health_check` = 'MaxDBConnectionThreshold' ";
		$query = $hDb->prepare( $sql );
		$query->execute();
		print BLUE . $sql . RESET . "\n" if $DEBUG;
		@results = $query->fetchrow_array();
		if (@results) {
			$threshold_pct = $results[0];
		}
		else {
			# If threshold is not defind in the sys_health table, us default value
			$threshold_pct = $defaultThreshold_pct;
		}

		$sql = "SHOW GLOBAL VARIABLES LIKE 'max_connections' ";
		$query = $hDb->prepare( $sql );
		$query->execute();
		print BLUE . $sql . RESET . "\n" if $DEBUG;
		@results = $query->fetchrow_array();
		if (@results) {
					# Max connections in mysql is max connections.
				$max_connections = $results[1];
		}
		else {
		$notes = sprintf("Query Failed for max connections: %s\n",$sql);
			$status = "FAIL" ;
			$errFlag = 1; 
		}

		$sql = "SHOW STATUS WHERE `variable_name` = 'Threads_connected' ";
		$query = $hDb->prepare( $sql );
		$query->execute();
		print BLUE . $sql . RESET . "\n" if $DEBUG;
		@results = $query->fetchrow_array();

		if (@results) {
			$curr_connections = $results[1];
		}
		else {
			$notes = sprintf("Query Failed for number of active connections: %s\n",$sql);
			$status = "FAIL" ;
			$errFlag = 1; 
		}

		if ($errFlag != 1) {

			my $current_percent = $curr_connections / $max_connections;

			if ($current_percent < $threshold_pct) {
				$status = "PASS" ;
				$notes = sprintf("Number of current database connections of %d, is below Threshold of %d. Max Connections is %d", 
				$curr_connections,$threshold_pct *  $max_connections, $max_connections) ;
			}
			else {
				$status = "FAIL" ;
				$notes = sprintf("Number of current database connections of %d, is above Threshold of %d, Max connections is %d",
				$curr_connections,$threshold_pct * $max_connections, $max_connections) ;
			}
		}
	}

	&PrintStatus($group,$name,$status,$notes);
}

sub system_check ($$$$$) {
	my ($name,$function,$param1,$param2,$param3) = @_;
	my $status = "UNKNOWN";
	my $notes = "";

	if ( $function eq "CpuWarn" ) {
		my $cpu_thresh;
		my $num_cpus = `/bin/cat /proc/cpuinfo | /bin/grep processor | /usr/bin/wc -l`;
		print BLUE . "/bin/cat /proc/cpuinfo | /bin/grep processor | /usr/bin/wc -l" . RESET . "\n" if $DEBUG;
		chomp($num_cpus);
		if ( $param1 ) {
			$cpu_thresh = $param1;
		}
		else {
			$cpu_thresh = $num_cpus -1;
		}
		my $load = `/bin/cat /proc/loadavg | /usr/bin/awk '{print \$2}'`;
		print BLUE . "/bin/cat /proc/loadavg | /usr/bin/awk '{print \$2}" . RESET . "\n" if $DEBUG;
		chomp($load);

		$status = "FAIL" if $load >= $cpu_thresh;
		$status = "PASS" if $load < $cpu_thresh;
		$notes = "Load average for the last 5 minutes is " . $load . ", the threshold is " . $cpu_thresh;
	}
	elsif ( $function eq "driveErrors" ) {
		my $drive_errors = `/usr/bin/tail -n 10000 /var/log/syslog | /bin/egrep -i -e '$param1' | /usr/bin/wc -l`;
		print BLUE . "/usr/bin/tail -n 10000 /var/log/syslog | /bin/egrep -i -e '" . $param1 . "' | /usr/bin/wc -l" . RESET . "\n" if $DEBUG;
		chomp($drive_errors);

		$status = "PASS" if $drive_errors == 0;
		$status = "FAIL" if $drive_errors > 0;
		$notes = "Found " . $drive_errors . " lines matching '" . $param1 . "' in /var/log/syslog";
	}
	elsif ( $function eq "DiskSpace" ) {        
        my $df = `/bin/df / | /bin/grep dev | /usr/bin/head -1 | /usr/bin/awk '{print \$5}'| awk -F '%' '{print \$1}'`;
        print BLUE . "/bin/df / | /bin/grep dev | /usr/bin/head -1 | /usr/bin/awk '{print \$5}'| awk -F '%' '{print \$1}'" . RESET . "\n" if $DEBUG;
        chomp($df);
        $status = ($df < $param1) ? "PASS" : "FAIL";
        $notes = "Drive usage is " . $df . "%, the threshold is " . $param1 . "%";
	}
	elsif ( $function eq "interfaceErrors" ) {
		($status, $notes) = interfaceErrors($param1);
	}

	elsif ( $function eq "memUsage" ) {
		my %regexs = (
			mem  => qr/cache:\s+(?<used>\d+)\s+(?<free>\d+)/s,
			swap => qr/Swap:\s+\d+\s+(?<used>\d+)\s+(?<free>\d+)/s,
		);

		`/usr/bin/free -b` =~ m/$regexs{ $param1 }/s;
		print BLUE . "/usr/bin/free -b" . RESET . "\n" if $DEBUG;

		my $used = $+{ used };
		my $free = $+{ free };
		my $total = $used + $free;

		if (defined($total)) {
			if ($total > 0) {
				my $percent_used = $used / $total * 100;
				$status = $percent_used < $param2 ? "PASS" : "FAIL";
				$notes = uc($param1) . " utilization is currently at " . int( $percent_used ) . "%, the threshold is " . $param2 . "%";
			}
		}
		else {
			$status = "FAIL";
			$notes = "divide by zero prevented.";
		}
	}
    elsif ( $function eq "ipmiAccess" ) {
        my $checkForIPMI = `cat /sys/class/dmi/id/sys_vendor`;
        if ( $checkForIPMI =~ /VMware/ )
        {    #if virual box there is no ipmi so pass
            $status = "PASS";
            $notes  = "Device is virtual, NO IPMI";
        }
        else    #if we have gotten here then the box should have IPMI
        {   #determine if running on 1U or 2U hardware
            my $hardwareType = '';
            if ( $checkForIPMI =~ /Supermicro/ ) {
                #print qq("$checkForIPMI" contains "Supermicro"\n);
                $hardwareType = "1U";
            }
            elsif ( $checkForIPMI =~ /Intel\sCorporation/ ) {
                #print qq("$checkForIPMI" contains "Intel Corporation"\n);
                $hardwareType = "2U";
            }
            my $ipmiaddr = `/usr/bin/ipmitool lan print | egrep "IP Address              :"`;
            if ( $ipmiaddr =~ /0\.0\.0\.0/ )
            { # if ipmi is not configured for shared then proceed with checking dedicated
                $ipmiaddr = `/usr/bin/ipmitool lan print 3 | egrep "IP Address              :"`;
                $ipmiaddr =~ /(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})/;
                $ipmiaddr = $1;
                my $nagiosResult =
                  `/usr/lib/nagios/plugins/check_http -S -I $ipmiaddr`;
                $status = "PASS", $notes = "IPMI can be reached via HTTPS"
                  if $nagiosResult =~ m/http ok:/io;
                $status = "FAIL", $notes = "IPMI can't be reached via HTTPS"
                  if $nagiosResult !~ m/http ok:/io;
            } ## end if ( $ipmiaddr =~ /0\.0\.0\.0/)

            else    # Parse then test attained IPMI address (applies for 2U shared, 1U shared, & 1U dedicated)
            {
                $ipmiaddr =~ /(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})/;
                $ipmiaddr = $1;
                if ($hardwareType eq "1U") {
                    #determine if shared or dedicated by using ipmitool
                    my $ipmi1UsharedCommand = `/usr/bin/ipmitool raw 0x30 0x70 0x0c 0`;
                    if ( $ipmi1UsharedCommand =~ /01/ ) {
                        #use 0.0.0.0 b/c quirk on Supermicro shared ipmi: check for https on ipmi ip fails falsely
                        $ipmiaddr = "0.0.0.0";
                    }
                    #determine if shared or dedicated by checking GRUB_SERIAL_COMMAND
                    #GRUB_SERIAL_COMMAND is less reliable than ipmitool since someone may change this
                    #my $grubSerialCommand = `sed -n '/^GRUB_SERIAL_COMMAND/p' /etc/default/grub`;
                    #if ( $grubSerialCommand =~ /--unit=3/ ) { # this would be for shared 1U
                }
                my $nagiosResult = `/usr/lib/nagios/plugins/check_http -S -I $ipmiaddr`;
                $status = "PASS", $notes = "IPMI can be reached via HTTPS"
                  if $nagiosResult =~ m/http ok:/io;
                $status = "FAIL", $notes = "IPMI can't be reached via HTTPS"
                  if $nagiosResult !~ m/http ok:/io;
            } ## end else [ if ( $ipmiaddr =~ /0\.0\.0\.0/)]
        } ## end else [ if ( $checkForIPMI =~ ...)]
    } ## end elsif ( $function eq "ipmiAccess")

	&PrintStatus($group,$name,$status,$notes);
}

sub PrintStatus ($$$$) {
	my ($group,$name,$status,$notes) = @_;

	if ( $status eq "FAIL" && $snmp eq "test" ) {
		print "CHECK\n";
		exit EXIT_BAD;
	}
	else {
		push @group_status, "$status";
	}

	if ( $snmp eq "results" ) {
		print $name . ": " . $status . "\n";
		print "\t" . $notes . "\n";
		if ($status eq "FAIL") {
			$hasFailed = 1;
		}
	}
	elsif ( !$output ) {
		print GREEN, $name . ": " . "$status\n", RESET if $status eq "PASS";
		print GREEN, $name . ": " . "$status\n", RESET if $status eq "N/A";
		print RED,   $name . ": " . "$status\n", RESET if $status eq "FAIL";
		print BLUE,  $name . ": " . "$status\n", RESET if $status eq "UNKNOWN";
		if ($status eq "FAIL") {
			$hasFailed = 1;
		}
		print "\t" . $notes . "\n";
	}
	elsif ( $snmp eq "test" ) {
		my $tests_to_run = keys %{$HealthConfig{ $group }};
		my $tests_ran = @group_status;

		if ( $tests_ran == $tests_to_run ) {
			my @check = grep(/FAIL/, @group_status);
			print "OK\n" if !@check;
			print "CHECK\n" if @check;
			if (@check) {
				$hasFailed = 1;
			}
		}
	}
}

sub parseInterfaces($) {
	my $interface = shift;
	my $interfaceStatsRaw = `/bin/cat /proc/net/dev | /bin/grep ":"`;
	#Inter-|   Receive                                                |  Transmit
	#face  |bytes    packets errs drop fifo frame compressed multicast|bytes    packets errs drop fifo colls carrier compressed
	#lo:  988980    3924    0    0    0     0          0         0   988980    3924    0    0    0     0       0          0
	my @interfaceStats = split(/\n/, $interfaceStatsRaw);
	foreach my $line (@interfaceStats) {
		$line =~ /[\s]+([a-z, A-Z, 0-9]+):[\s]+([0-9]+)[\s]+([0-9]+)[\s]+([0-9]+)[\s]+([0-9]+)[\s]+([0-9]+)[\s]+([0-9]+)[\s]+([0-9]+)[\s]+([0-9]+)[\s]+([0-9]+)[\s]+([0-9]+)[\s]+([0-9]+)[\s]+([0-9]+)[\s]+([0-9]+)[\s]+([0-9]+)[\s]+([0-9]+)[\s]+([0-9]+)/;
#		Leaving in this commented section as a reference of what $1, $4, $12 actually mean.
# 		my %return;
# 		$return{'interface'} = $1;
# 		$return{'r-bytes'} = $2;
# 		$return{'r-packets'} = $3;
# 		$return{'r-errs'} = $4;
# 		$return{'r-drop'} = $5;
# 		$return{'r-fifo'} = $6;
# 		$return{'r-frame'} = $7;
# 		$return{'r-compressed'} = $8;
# 		$return{'r-multicast'} = $9;
# 		$return{'t-bytes'} = $10;
# 		$return{'t-packets'} = $11;
# 		$return{'t-errs'} = $12;
# 		$return{'t-drop'} = $13;
# 		$return{'t-fifo'} = $14;
# 		$return{'t-frame'} = $15;
# 		$return{'t-compressed'} = $16;
# 		$return{'t-multicast'} = $17;
		if ($1 eq $interface) {
			return saveInterfaceData($1, $4, $12, getCollisions($1));
		}
	}
}

sub getCollisions($) {
	my $interface = shift;
	my $command = "/sbin/ifconfig " . $interface . " | /bin/grep collisions: | /bin/sed -e \"s: ::g\" | /bin/egrep \"[0-9]+\" -o | head -n1";
	return `$command`;
}

sub getInterfaceData($) {
	my $interface = shift;
	my $sql = "SELECT `tx_errors_current`, `rx_errors_current`, `collisions_current`, `error`"
	. " FROM sys_health_ethernet WHERE interface = '" . $interface . "'";
	print BLUE . $sql . RESET . "\n" if $DEBUG;
	my $query = $hDb->prepare( $sql );
	$query->execute();
	print BLUE . $sql . RESET . "\n" if $DEBUG;
	my @results = $query->fetchrow_array() or return ([]);
	return (@results);
}

sub saveInterfaceData($$$$) {
	my $interface = shift;
	my $rxErrors = shift;
	my $txErrors = shift;
	my $collisions = shift;
	my $error;
	my @data = getInterfaceData($interface);
	# Check if the database is down or something and we can't continue.
	if (@data) {

		# If it was errored before, leave it as an error.
		# TODO: We would really look into this again at some point as it seems pointless to keep it locked down until an AE fixes it.
		# TODO: If the MySQL server is still up then this could be resolved using a statemachine.
		# TODO: If its not up then we have much larger problems than if the interfaces are throwing errors as the server is likely engulfed in flames.

		if ($data[3] == 1) {
			my $txThreshold = fetchHealthCheckValue("Interface: TX Threshold");
			my $rxThreshold = fetchHealthCheckValue("Interface: RX Threshold");
			my $colThreshold = fetchHealthCheckValue("Interface: Collision Threshold");
			if ($data[0] > $txThreshold) {
				$error = 1;
			}
			elsif ($data[1] > $rxThreshold) {
				$error = 1;
			}
			elsif ($data[2] > $colThreshold) {
				$error = 1;
			}
			else {
				$error = $data[3];
			}
		}
		else {
			$error = $data[3];
		}

		my $sql = "REPLACE INTO `sys_health_ethernet`"
		. " (`interface`, `tx_errors_current`, `tx_errors_previous`, `rx_errors_current`, `rx_errors_previous`, `collisions_current`, `collisions_previous`, `error`)"
		. " VALUES ('" . $interface . "', " . $txErrors . ", " . $data[0] . ", " . $rxErrors . ", " . $data[1] . ", " . $collisions . ", " . $data[2] . ", " . $error . ")";
		my $query = $hDb->prepare( $sql );
		$query->execute();
		print BLUE . $sql . RESET . "\n" if $DEBUG;
		return 1;
	}
	return 0;
}

sub setInterfaceFailureFlag($) {
	my $interface = shift;
	my $sql = "UPDATE `sys_health_ethernet` SET `error` = '1' WHERE `interface` = '" . $interface . "'";
	my $query = $hDb->prepare( $sql );
	$query->execute();
	print BLUE . $sql . RESET . "\n" if $DEBUG;
}

sub interfaceErrors($) {
	my $interface = shift;
	my $status;
	my $notes;

	# Parsed and saved to the database.
	my $passFail = parseInterfaces($interface);
	if ($passFail) {
		my @data = getInterfaceData($interface);
		if (@data) {
			if ($data[3] == 0) {
				$status = "PASS";
				$notes = "The errors are within the defined thresholds.";
			}
			else {
				$status = "FAIL";
				$notes = "The error flag has been set on " . $interface . " to prevent flapping, you will need to manually disable it.";
			}
		}
		else {
			$status = "FAIL";
			$notes = "Unable to get interface data for " . $interface;
		}
	}
	else {
		$status = "FAIL";
		$notes = "Unable to parse interface data for " . $interface;
	}
	return ($status, $notes);
}
