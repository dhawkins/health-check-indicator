#! /bin/bash
#
# csdiag
#
# Copyright Shoregroup Inc, 2016

# get connection params from db_logins
CONNECT_PARAMS=`/var/www/CaseSentry/bin/shell_connect_vars.php Main`

if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
echo "This script will provide diagnostic information on CaseSentry."
echo "Running this script as root will provide information on network"
echo "interfaces and disk diagnostics."
echo "Usage"
echo "   $0       Run basic script"
echo "   $0 -v    Run with additional diagnostic information"
echo "   $0 -h    Display the help information"
echo ""
exit 0
fi

export PRODUCT_TYPE=`/usr/bin/mysql $CONNECT_PARAMS -N -B -e"SELECT value from CaseSentryConfig where parm='PRODUCT_TYPE'"  2> /var/log/health-check-indicator/csdiag.log`
if [ "$PRODUCT_TYPE" = 'CROS' ]; then
        export PRODUCT_NAME='CISCO APPLIANCE'
else
        export PRODUCT_NAME='CASE SENTRY'
fi

if [ "$1" = "-v" ]; then
echo "$PRODUCT_NAME EXTENDED DIAGNOSTIC REPORT"
else
echo "$PRODUCT_NAME DIAGNOSTIC REPORT"
fi
echo "Device: `hostname -f`"
echo "Generated: `date`"
echo ""
echo ""
echo "***** RUNNING ALL_STATUS *****"
/usr/local/bin/all_status
echo ""
echo "***** UPDATING HEALTH_CHECK CONFIGS *****"
sudo /usr/local/bin/writeHealthCheckCFG.pl --auto-update
echo ""
echo "***** RUNNING HEALTH_CHECK *****"
sudo /usr/local/bin/health_checks.pl --test-group=all
echo ""
echo "***** RUNNING AUTH CHECK *****"
/var/www/CaseSentry/bin/AuthCheck
if [ "$1" != "-v" ]; then
echo ""
echo "Please check log file for messages."
echo "End of basic report.  Run with -v for extended system information."
exit 0;
fi

echo ""
echo "***** IFCONFIG *****"
if [ $(id -u) -eq 0 ]; then
/sbin/ifconfig
else
echo "Must run this script as root for ifconfig information."
fi
echo ""
echo "***** ROUTING TABLE *****"
/bin/netstat -rn
echo ""
echo "***** TESTING VPN *****"
PING_CHECK=192.168.2.243
/bin/ping -c 4 $PING_CHECK 1>/dev/null 2>&1
if (( $? ));
then
	echo "VPN appears to be down."
else
	echo "VPN appears to be up."
fi
echo ""
echo "***** DISK USAGE *****"
/bin/df -h
echo ""
echo "***** UPTIME *****"
/usr/bin/w -s
echo ""
echo "***** LAST 5 LOGINS *****"
/usr/bin/last -n 5
echo ""
echo "***** DISK DRIVE DIAGNOSTICS *****"
if [ $(id -u) -eq 0 ]; then
/usr/sbin/smartctl /dev/hda -a
else
echo "Must run this script as root for disk drive information."
fi
echo ""
echo ""
echo "Please check log file for messages."
echo "End of extended report."
