#!/usr/bin/perl

# @version $Id: sendHealthEmail.pl 43994 2013-03-08 15:32:31Z csteffler $
# @copyright 1999,2016, ShoreGroup, Inc.

use warnings;
use strict;
use DBI;
use Getopt::Long;
use lib "/usr/lib/health-indicators/perl";
use ConnectVars;

my $hDb = getConnection('Main') or die "Exiting - Main database connection failed: $DBI::errstr";

# Assign global variables
my $DEBUG;
my $auto_update;
my $system_type;
my $help;

GetOptions( "debug"       => \$DEBUG,
            "auto-update" => \$auto_update,
            "help"        => \$help);

# Check arguments and act
if ( $help ) {
	&print_usage;
}
elsif ( !$auto_update ) {
	$system_type = &get_system_type_from_user;
}
elsif ( $auto_update )  {
	$system_type = &get_system_type_from_file;
}
else {
	&print_usage;
}

if ( $system_type !~ m/[A-Z0-9]/io) {
	die "System Type not specified in previous config, please run without --auto-update flag\n";
}

print "System Type: " . $system_type . "\n" if $DEBUG;

# Update CFG File for Health Check
print " -- Opening file /etc/health-indicators/HealthCheckConfig.tsv\n" if $DEBUG;
open HEALTHCHECK, ">", "/etc/health-indicators/HealthCheckConfig.tsv" or die $!;
print " -- Writing to /etc/health-indicators/HealthCheckConfig.tsv\n" if $DEBUG;

print HEALTHCHECK "SystemType=" . $system_type . "\n";

my $system_type_sql = $system_type;
$system_type_sql =~ s/(^|$)/'/g;
$system_type_sql =~ s/,/','/g;

my $sql = "SELECT `name`, `group_name`, `function`, `param1`, `param2`, `param3`"
. " FROM `Healthcheck_Indicator`.`csstatus_system_types_objects`"
. " WHERE `type_id` IN ( SELECT `id` FROM `Healthcheck_Indicator`.`csstatus_system_types` WHERE `system_type` IN (" . $system_type_sql . ") )"
. " ORDER BY `group_name`, `name`";

my $query = $hDb->prepare( $sql );
$query->execute();
while ( my ( $name, $group_name, $function, $param1, $param2, $param3 ) = $query->fetchrow_array() ) {
	print HEALTHCHECK $group_name . "\t" . $name . "\t" . $function . "\t" . $param1 . "\t" . $param2 . "\t" . $param3 . "\n";
}

print " -- Closing file /etc/health-indicators/HealthCheckConfig.tsv\n" if $DEBUG;
close HEALTHCHECK or die $!;

print "Complete\n" if $DEBUG;

exit(0);




########## End Main ##########
sub print_usage {
	die "Usage: ./writeHealthCheckCFG.pl [--auto-update]"
	, "\n"
	, "\n"
	, "\t--help            Display this message\n"
	, "\t--debug           Enable debugging\n"
	, "\t--auto-update     Auto updates Health Indicator CFG file based on current device type\n";
}

sub get_system_type_from_user() {
	my %type;
	my @roles;
	my @role_string;

	my $sql = 'SELECT `id`,`system_type` FROM `Healthcheck_Indicator`.`csstatus_system_types`';
	my $query = $hDb->prepare( $sql );
	$query->execute();
	while ( my ($id,$system_type) = $query->fetchrow_array() ) {
		$type{ $id } = $system_type;
	}

	print "System Types:\n";

	foreach my $id ( sort keys %type ) {
		print $id . ") - " . $type{ $id } . "\n";
	}

	print "Please select a system type from above. For multiple role systems, seperate types by comma (ex. 1,2):\n";

	my $input = <STDIN>;
	chomp ($input);

	if ( $input =~ m/,/o ) {
		@roles = split(",", $input);
		foreach my $role ( sort @roles ) {
			if ( not defined $type{ $role } ) {
				&get_system_type_from_user; exit;
			}
			push @role_string, $type{ $role };
		}
		print "DEBUG: " . join(",", @role_string) . "\n\n";
		return join(",", @role_string);
	}
	else {
		if ( not defined $type{ $input } ) {
			&get_system_type_from_user;
		exit;
		}
		return $type{ $input }; 
	}
}

sub get_system_type_from_file() {
	my %type;

	unless ( -e "/etc/health-indicators/HealthCheckConfig.tsv" ) {
		die "writeHealthCheckCFG.pl needs to be ran without the '--auto-update' flag for initial install\n";
	}

	open CURRENTCONFIG, "<", "/etc/health-indicators/HealthCheckConfig.tsv" or die $!;
	my @lines = <CURRENTCONFIG>;
	close CURRENTCONFIG or die $!;

	my $read_type = $lines[0];
	$read_type =~ s/(SystemType=|\n)//go;
	return $read_type;
}
