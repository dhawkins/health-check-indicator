This work was packaged for Debian by:

    Dan Hawkins <daniel.hawkins@shoregroup.com> on Mon, 14 Mar 2016 11:00:27 -0400

Copyright:

    Copyright (C) 2012,2016 ShoreGroup, Inc.

License:

    SOFTWARE LICENSE AGREEMENT

    PLEASE READ THIS SOFTWARE LICENSE AGREEMENT CAREFULLY BEFORE ACCESSING AND
    USING THE SOFTWARE. UPON USE OF THIS SOFTWARE OR EQUIPMENT THAT CONTAINS
    THIS SOFTWARE, YOU ARE CONSENTING TO BE BOUND BY THIS AGREEMENT. IF YOU DO
    NOT AGREE TO ALL OF THE TERMS OF THIS AGREEMENT, RETURN THE SOFTWARE TO
    SHOREGROUP, INC.

    Single User License Grant: ShoreGroup, Inc. ("ShoreGroup”) and its suppliers
    grant to Customer ("Customer") a nonexclusive and nontransferable license to
    use the ShoreGroup software ("Software") in object code form solely on a
    single central processing unit owned or leased by Customer or otherwise
    embedded in equipment provided by ShoreGroup.

    Multiple-Users License Grant: ShoreGroup, Inc. ("ShoreGroup") and its
    suppliers grant to Customer ("Customer") a nonexclusive and nontransferable
    license to use the ShoreGroup software ("Software") in object code form:
    (i) installed in a single location on a hard disk or other storage device of
    up to the number of computers owned or leased by Customer for which Customer
    has paid a license fee ("Permitted Number of Computers"); or
    (ii) provided the Software is configured for network use, installed on a
    single file server for use on a single local area network for either (but
    not both) of the following purposes: (a) permanent installation onto a hard
    disk or other storage device of up to the Permitted Number of Computers; or
    (b) use of the Software over such network, provided the number of computers
    connected to the server does not exceed the Permitted Number of Computers.
    Customer may only use the programs contained in the Software (i) for which
    Customer has paid a license fee (or in the case of an evaluation copy, those
    programs Customer is authorized to evaluate) and (ii) for which Customer has
    received a product authorization key ("PAK"). Customer grants to ShoreGroup
    or its independent accountants the right to examine its books, records and
    accounts during Customer's normal business hours to verify compliance with
    the above provisions. In the event such audit discloses that the Permitted
    Number of Computers is exceeded, Customer shall promptly pay to ShoreGroup
    the appropriate licensee fee for the additional computers or users. At
    ShoreGroup's option, ShoreGroup may terminate this license for failure to
    pay the required license fee.

    Customer may make one (1) archival copy of the Software provided Customer
    affixes to such copy all copyright, confidentiality, and proprietary notices
    that appear on the original.

    EXCEPT AS EXPRESSLY AUTHORIZED ABOVE, CUSTOMER SHALL NOT: COPY, IN WHOLE
    OR IN PART, SOFTWARE OR DOCUMENTATION; MODIFY THE SOFTWARE; REVERSE COMPILE
    OR REVERSE ASSEMBLE ALL OR ANY PORTION OF THE SOFTWARE; OR RENT, LEASE,
    DISTRIBUTE, SELL, OR CREATE DERIVATIVE WORKS OF THE SOFTWARE.

    Customer agrees that aspects of the licensed materials, including the
    specific design and structure of individual programs, constitute trade
    secrets and/or copyrighted material of ShoreGroup. Customer agrees not to
    disclose, provide, or otherwise make available such trade secrets or
    copyrighted material in any form to any third party without the prior
    written consent of ShoreGroup. Customer agrees to implement reasonable
    security measures to protect such trade secrets and copyrighted material.
    Title to Software and documentation shall remain solely with ShoreGroup.

DISCLAIMER. EXCEPT AS SPECIFIED IN THIS WARRANTY, ALL EXPRESS OR IMPLIED
    CONDITIONS, REPRESENTATIONS, AND WARRANTIES INCLUDING, WITHOUT LIMITATION,
    ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE,
    NONINFRINGEMENT OR ARISING FROM A COURSE OF DEALING, USAGE, OR TRADE
    PRACTICE, ARE HEREBY EXCLUDED TO THE EXTENT ALLOWED BY APPLICABLE LAW.

    IN NO EVENT WILL SHOREGROUP OR ITS SUPPLIERS BE LIABLE FOR ANY LOST REVENUE,
    PROFIT, OR DATA, OR FOR SPECIAL, INDIRECT, CONSEQUENTIAL, INCIDENTAL, OR
    PUNITIVE DAMAGES HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY
    ARISING OUT OF THE USE OF OR INABILITY TO USE THE SOFTWARE EVEN IF SHOREGROUP
    OR ITS SUPPLIERS HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. In no
    event shall ShoreGroup's or it’s suppliers' liability to Customer, whether in
    contract, tort (including negligence), or otherwise, exceed the price paid by
    Customer. The foregoing limitations shall apply even if the above-stated
    warranty fails of its essential purpose. SOME STATES DO NOT ALLOW LIMITATION
    OR EXCLUSION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES.

    CUSTOMER ACKNOWLEDGES THAT THE SOFTWARE IS NOT DESIGNED OR INTENDED BY
    SHOREGROUP FOR USE OR RESALE IN, OR FOR INCORPORATION INTO PRODUCTS OR
    SERVICES USED IN HIGH RISK ACTIVITIES.  High Risk Activities means on-line
    control equipment in environments requiring fail-safe performance, including
    but not limited to emergency or E911 applications or services, the operation
    of nuclear facilities, aircraft navigation or aircraft communication systems,
    air traffic control, direct life support machines or weapons systems, etc.
    in which the failure of the products or Services could lead directly to
    death, personal injury, or severe physical or environmental damage.
    SHOREGROUP SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF ANY
    KIND WITH RESPECT TO THE USE OF THE SOFTWARE OR RELATED SERVICES IN
    CONNECTION WITH ANY HIGH RISK ACTIVITY.  CUSTOMER FURTHER ACKNOWLEDGES THAT
    THE ACT OF MONITORING HIGH RISK SYSTEMS FOR CUSTOMER USE DOES NOT
    CONSTITUTE SHOREGROUP SUPPORT OF SUCH SYSTEMS OR THE GUARANTEE THAT THE
    SYSTEMS WILL BE AVAILABLE DURING AN EMERGENCY.

    This License is effective during the Customer’s managed service subscription
    or until terminated.  Customer may terminate this License at any time by
    destroying all copies of Software including any documentation. This License
    will terminate immediately without notice from ShoreGroup if Customer fails
    to comply with any provision of this License. Upon termination, Customer
    must destroy all copies of Software.

    Software, including technical data, is subject to U.S. export control laws,
    including the U.S. Export Administration Act and its associated regulations,
    and may be subject to export or import regulations in other countries.
    Customer agrees to comply strictly with all such regulations and
    acknowledges that it has the responsibility to obtain licenses to export,
    re-export, or import Software.

    This License shall be governed by and construed in accordance with the laws
    of the State of New York, United States of America, as if performed wholly
    within the state and without giving effect to the principles of conflict of
    law. If any portion hereof is found to be void or unenforceable, the
    remaining provisions of this License shall remain in full force and effect.
    This License constitutes the entire License between the parties with respect
    to the use of the Software.

    Restricted Rights - ShoreGroup's software is provided to non-DOD agencies
    with RESTRICTED RIGHTS and its supporting documentation is provided with
    LIMITED RIGHTS. Use, duplication, or disclosure by the Government is subject
    to the restrictions as set forth in subparagraph "C" of the Commercial
    Computer Software - Restricted Rights clause at FAR 52.227-19. In the event
    the sale is to a DOD agency, the government's rights in software, supporting
    documentation, and technical data are governed by the restrictions in the
    Technical Data Commercial Items clause at DFARS 252.227-7015 and
    DFARS 227.7202. Manufacturer is ShoreGroup, Inc. 460 West 35th Street,
    New York, NY 10001.

The Debian packaging is:

    Copyright (C) 2016 Shoregroup, Inc.

